## PROJECT DETAILS

Software package includes an FPGA implementation of the Conjugate Gradient algorithm using a particular problem of the Dirac-Wilson operator.

The framework allows for a simple implementation of the linear operators accelerated using FPGA resources.

The project has been developed for Xilinx Alveo U280 cards using OpenCL and C++.


## COMPILATION


The project can be compiled as a standalone, host-only executable (G++) or as full, hardware-accelerated solution (Xilinx Vitis).

  

### G++

  

Use ```make local``` to build the sources with SOFTONLY flag, which allows to evaluate the solution without hardware or Vitis.

  

### XILINX VITIS

  

First source the settings for the environment:

  

```source PATH_TO_VITIS\settings64.sh```

  

Then run the IDE, create the project for Alveo U280 with the shell xilinx_u280_xdma_201920_1 and import all sources from the src directory.

Select function1 to accelerate and apply the following compilation flags:

  

* binary_container_1:

```--xp vivado_prop:run.impl.STEPS.PLACE_DESIGN.ARGS.DIRECTIVE=Congestion_SpreadLogic_High --sp function1_1.m_axi_gmem0:HBM[0] --sp function1_1.m_axi_gmem1:HBM[1] --sp function1_1.m_axi_gmem2:HBM[2] --sp function1_1.m_axi_gmem3:HBM[3]```

  

* kernels:

```--xp prop:solution.kernel_compiler_margin=95```

## EVALUATION


Run the executable file generated with Makefile and provide input data file as an argument.

In Vitis use hardware emulation to verify the execution trace, dataflow and estimated performance using the generated reports.

Build the entire solution up to the bitstream stage (can take up to 15 hours, depending on compilation system) and use Nimbix or Amazon AWS clouds to run the project on accelerated hardware.


## AUTHORS

Grzegorz Korcyl (grzegorz.korcyl@uj.edu.pl)

Piotr Korcyl (piotr.korcyl@uj.edu.pl)
