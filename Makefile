.PHONY: help

help::
	@echo "Makefile Usage:"	
	@echo ""
	@echo "  make local"
	@echo "      Command to compile the entire project with G++."
	@echo ""
	@echo "  make clean "
	@echo "      Command to remove the generated non-hardware files."
	@echo ""
	@echo "  make cleanall"
	@echo "      Command to remove all the generated files."
	@echo ""


PRJNAME := su3_alveo

ABS_COMMON_REPO = $(shell readlink -f $(COMMON_REPO))

TARGETS := hw
TARGET := $(TARGETS)
DEVICE := $(DEVICES)
XCLBIN := ./xclbin

VITIS_PLATFORM = xilinx_u280_xdma_201920_1
VITIS_PLATFORM_PATH = $(VITIS_PLATFORM)

DSA :=
BUILD_DIR := ./work

CXX := g++
VPP := $(XILINX_VITIS)/bin/v++

RM = rm -f
RMDIR = rm -rf

ECHO:= @echo


###################################################################
# XCPP COMPILER FLAGS
######################################################################
opencl_CXXFLAGS += -g -I./ -I$(XILINX_XRT)/include -I$(XILINX_VIVADO)/include -Wall -O0 -g -std=c++14
# The below are linking flags for C++ Comnpiler
opencl_LDFLAGS += -L$(XILINX_XRT)/lib -lOpenCL -lpthread -lrt -lstdc++

###########################################################################
#END OF GENERATION OF XO
##########################################################################

CXXFLAGS += $(opencl_CXXFLAGS)
LDFLAGS += $(opencl_LDFLAGS)

# Host compiler global settings
CXXFLAGS += -fmessage-length=0
LDFLAGS += -lrt -lstdc++

# Kernel compiler global settings
CLFLAGS += -t $(TARGET) --platform $(DEVICE) --save-temps

CP = cp -rf

.PHONY: local
local:
	@mkdir -p $(BUILD_DIR)
	$(CXX) $(CXXFLAGS) -c ./src/fpga_kernel.cpp -o $(BUILD_DIR)/fpga_kernel.o -D SOFTONLY
	$(CXX) $(CXXFLAGS) -c ./src/fpga_engine.cpp -o $(BUILD_DIR)/fpga_engine.o -D SOFTONLY
	$(CXX) $(CXXFLAGS) -c ./src/function1.cpp -o $(BUILD_DIR)/function1.o -D SOFTONLY
	$(CXX) $(CXXFLAGS) $(BUILD_DIR)/fpga_kernel.o $(BUILD_DIR)/fpga_engine.o $(BUILD_DIR)/function1.o ./src/main.cpp -o hpcg_fpga $(LDFLAGS) -D SOFTONLY

.PHONY: exe
exe: $(EXECUTABLE)



# Cleaning stuff
clean:
	-$(RMDIR) $(BUILD_DIR)
	@rm hpcg_fpga

cleanall: clean
	-$(RMDIR) $(BUILD_DIR)
	@rm hpcg_fpga

#######################################################################
# RTL Kernel only supports Hardware and Hardware Emulation.
# THis line is to check that
#########################################################################
ifneq ($(TARGET),$(findstring $(TARGET), hw hw_emu))
$(warning WARNING:Application supports only hw hw_emu TARGET. Please use the target for running the application)
endif

###################################################################
#check the devices avaiable
########################################################################

check-devices:
ifndef DEVICE
	$(error DEVICE not set. Please set the DEVICE properly and rerun. Run "make help" for more details.)
endif

############################################################################
# check the VITIS environment
#############################################################################

ifndef XILINX_VITIS
$(error XILINX_VITIS variable is not set, please set correctly and rerun)
endif
