/* 
 * This file is part of the HPCG_FPGA distribution (https://bitbucket.org/fpgafais/hpcg_fpga).
 * Copyright (c) 2020 G. Korcyl, P. Korcyl
 * 
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU General Public License as published by  
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * 
 * File: fpga_engine.h
 * Authors: G. Korcyl, P. Korcyl
 * Contact: grzegorz.korcyl@uj.edu.pl, piotr.korcyl@uj.edu.pl
 * 
 * Version: 1.0
 * 
 * Description:
 * Class representing hardware accelerator card and basic functions for initializing
 * and calling kernels in FPGA
 * 
 */

#ifndef __H_FPGA_ENGINE__
#define __H_FPGA_ENGINE__


#include <iostream>
#include <fstream>
#include <vector>
#include <string>


#define CL_HPP_CL_1_2_DEFAULT_BUILD
#define CL_HPP_TARGET_OPENCL_VERSION 120
#define CL_HPP_MINIMUM_OPENCL_VERSION 120
#define CL_HPP_ENABLE_PROGRAM_CONSTRUCTION_FROM_ARRAY_COMPATIBILITY 1

#include <CL/cl2.hpp>
#include <CL/cl_ext.h>


//TARGET_DEVICE macro needs to be passed from gcc command line
#if defined(SDX_PLATFORM) && !defined(TARGET_DEVICE)
	#define STR_VALUE(arg)      #arg
	#define GET_STRING(name) STR_VALUE(name)
	#define TARGET_DEVICE GET_STRING(SDX_PLATFORM)
#endif


#include "fpga_kernel.h"

template <class T_low, class T_high>
class fpga_engine {
public:

	// OpenCL components for managing the kernels
	std::vector<cl::Device> devices;
	cl::Device device;
	cl::Context context;
	cl::CommandQueue queue;
	cl::Program program;

	// Vectors of kernels loaded into the device
	std::vector<fpga_kernel<T_low>*> kernels_low;
	std::vector<fpga_kernel<T_high>*> kernels_high;

	int resolution; // 0 - high, 1 - low

public:

	int init_device();
	int load_bin_file(std::string file_name);
	
	fpga_kernel<T_low>* create_kernel_low(std::string function_name);
	fpga_kernel<T_high>* create_kernel_high(std::string function_name);

	fpga_kernel<T_low>* get_kernel_low(int i);
	fpga_kernel<T_high>* get_kernel_high(int i);
	
	int enqueue_all_outputs();
	int enqueue_all_inputs();
	int execute_queue();
	int enqueue_kernels();

	void select_active_resolution(int i) { resolution = i; }

};

/*
 *	Hardware board initlization and command queue creation
 */
template <class T_low, class T_high>
int fpga_engine<T_low, T_high>::init_device() {
	//traversing all Platforms To find Xilinx Platform and targeted
	//Device in Xilinx Platform
	bool found_device = false;

	std::vector<cl::Platform> platforms;

#ifndef SOFTONLY
    const char *target_device_name = "xilinx_u280_xdma_201920_1"; //TARGET_DEVICE;
#endif

#ifdef SOFTONLY
    const char *target_device_name = "ALVEO";
#endif

	cl::Platform::get(&platforms);
	for(size_t i = 0; (i < platforms.size() ) & (found_device == false) ;i++){
		cl::Platform platform = platforms[i];
		std::string platformName = platform.getInfo<CL_PLATFORM_NAME>();
		if ( platformName == "Xilinx"){
			devices.clear();
			platform.getDevices(CL_DEVICE_TYPE_ACCELERATOR, &devices);

			//Traversing All Devices of Xilinx Platform
			for (size_t j = 0 ; j < devices.size() ; j++){
				device = devices[j];
				std::string deviceName = device.getInfo<CL_DEVICE_NAME>();
				std::cout<<deviceName<<std::endl;
				if (deviceName == target_device_name){
					found_device = true;
					break;
				}
			}
		}
	}
	if (found_device == false){
	   std::cout << "Error: Unable to find Target Device "<< target_device_name << std::endl;
	   return EXIT_FAILURE;
	}

	std::cerr<<"Device created"<<std::endl;

	cl_int err;
	// Creating Context and Command Queue for selected device
	context = cl::Context(device, NULL, NULL, NULL, &err);
	OCL_CHECK(err, err = err);
	queue = cl::CommandQueue(context, device, CL_QUEUE_PROFILING_ENABLE, &err); // | CL_QUEUE_OUT_OF_ORDER_EXEC_MODE_ENABLE, &err);
	OCL_CHECK(err, err = err);

	std::cerr<<"Queue created for device "<<device.getInfo<CL_DEVICE_NAME>()<<std::endl;

	select_active_resolution(0);
	std::cerr<<"Resolution set to "<<resolution<<std::endl;

	return 0;
}

/*
 *	Reads the .xclbin file and configures the accelerator board FPGA with generated logic
 */
template <class T_low, class T_high>
int fpga_engine<T_low, T_high>::load_bin_file(std::string file_name) {
	// Load xclbin
	std::cerr << "Loading: '" << file_name << "'\n";
	std::ifstream bin_file(file_name.c_str(), std::ifstream::binary);
	bin_file.seekg (0, bin_file.end);
	unsigned nb = bin_file.tellg();
	bin_file.seekg (0, bin_file.beg);
	char *buf = new char [nb];
	bin_file.read(buf, nb);

	std::cerr<<"Bin file loaded"<<std::endl;

	// Creating Program from Binary File
	cl::Program::Binaries bins;
	bins.push_back( {buf,nb} );
	devices.resize(1);

	cl_int err;
	program = cl::Program(context, devices, bins, NULL, &err);
	OCL_CHECK(err, err = err);

	std::cerr<<"Program created"<<std::endl;

	return 0;
}

/*
 *	Creates an instance of low resolution kernel
 */
template<class T_low, class T_high>
fpga_kernel<T_low>* fpga_engine<T_low, T_high>::create_kernel_low(std::string function_name) {
	fpga_kernel<T_low>* k = new fpga_kernel<T_low>(&program, &context, function_name.c_str());
	kernels_low.push_back(k);

	std::cerr<<"Kernel created"<<std::endl;

	return k;
}

/*
 *	Creates an instance of high resolution kernel
 */
template<class T_low, class T_high>
fpga_kernel<T_high>* fpga_engine<T_low, T_high>::create_kernel_high(std::string function_name) {
	fpga_kernel<T_high>* k = new fpga_kernel<T_high>(&program, &context, function_name.c_str());
	kernels_high.push_back(k);

	std::cerr<<"Kernel created"<<std::endl;

	return k;
}

/*
 *	Returns a given instance of low resolution kernel
 */
template<class T_low, class T_high>
fpga_kernel<T_low>* fpga_engine<T_low, T_high>::get_kernel_low(int i) {
	return kernels_low[i];
}

/*
 *	Returns a given instance of high resolution kernel
 */
template<class T_low, class T_high>
fpga_kernel<T_high>* fpga_engine<T_low, T_high>::get_kernel_high(int i) {
	return kernels_high[i];
}

/*
 *	Calls CommandQueue.finish() to execute all queued commands
 */
template <class T_low, class T_high>
int fpga_engine<T_low, T_high>::execute_queue() {
	cl_int err;
	OCL_CHECK(err, err = queue.flush());
	OCL_CHECK(err, err = queue.finish());
	return err;
}

/*
 *	Appends the CommandQueue with migration of output buffers for all kernels
 */
template <class T_low, class T_high>
int fpga_engine<T_low, T_high>::enqueue_all_outputs() {
	int ctr = 0;
	cl_int err;

	if (resolution == 0) {
		typename std::vector<fpga_kernel<T_high>*>::iterator it;
		for (it = kernels_high.begin(); it != kernels_high.end(); it++) {
			for(int i = 0; i < (*it)->get_out_buffers_number(); i++) {
				cl::Buffer& buf_ref = *((*it)->get_out_buffer(i)->buffer);
				OCL_CHECK(err, err = queue.enqueueMigrateMemObjects( {buf_ref}, 0));
				ctr++;
			}
		}
	}
	else if (resolution == 1) {
		typename std::vector<fpga_kernel<T_low>*>::iterator it;
		for (it = kernels_low.begin(); it != kernels_low.end(); it++) {
			for(int i = 0; i < (*it)->get_out_buffers_number(); i++) {
				cl::Buffer& buf_ref = *((*it)->get_out_buffer(i)->buffer);
				OCL_CHECK(err, err = queue.enqueueMigrateMemObjects( {buf_ref}, 0));
				ctr++;
			}
		}
	}
	return ctr;
}

/*
 *	Appends the CommandQueue with migration of input buffers for all kernels
 */
template <class T_low, class T_high>
int fpga_engine<T_low, T_high>::enqueue_all_inputs() {
	int ctr = 0;
	cl_int err;

	if (resolution == 0) {
		typename std::vector<fpga_kernel<T_high>*>::iterator it;
		for (it = kernels_high.begin(); it != kernels_high.end(); it++) {
			for(int i = 0; i < (*it)->get_in_buffers_number(); i++) {
				cl::Buffer& buf_ref = *((*it)->get_in_buffer(i)->buffer);
				OCL_CHECK(err, err = queue.enqueueMigrateMemObjects( {buf_ref}, CL_MIGRATE_MEM_OBJECT_HOST));
				ctr++;
			}
		}
	}
	else if (resolution == 1) {
		typename std::vector<fpga_kernel<T_low>*>::iterator it;
		for (it = kernels_low.begin(); it != kernels_low.end(); it++) {
			for(int i = 0; i < (*it)->get_in_buffers_number(); i++) {
				cl::Buffer& buf_ref = *((*it)->get_in_buffer(i)->buffer);
				OCL_CHECK(err, err = queue.enqueueMigrateMemObjects( {buf_ref}, CL_MIGRATE_MEM_OBJECT_HOST));
				ctr++;
			}
		}
	}
	return ctr;
}

/*
 *	Appends the CommandQueue with start call to all kernels
 */
template <class T_low, class T_high>
int fpga_engine<T_low, T_high>::enqueue_kernels() {
	int ctr = 0;
	cl_int err;

	if (resolution == 0) {
		typename std::vector<fpga_kernel<T_high>*>::iterator it;
		for (it = kernels_high.begin(); it != kernels_high.end(); it++) {
			cl::Kernel& a = *(*it)->get_kernel();
			OCL_CHECK(err, err = queue.enqueueTask(a));
			ctr++;
		}
	}
	else if (resolution == 1) {
		typename std::vector<fpga_kernel<T_low>*>::iterator it;
		for (it = kernels_low.begin(); it != kernels_low.end(); it++) {
			cl::Kernel& a = *(*it)->get_kernel();
			OCL_CHECK(err, err = queue.enqueueTask(a));
			ctr++;
		}
	}
	return ctr;
}


#endif
