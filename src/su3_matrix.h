/* 
 * This file is part of the HPCG_FPGA distribution (https://bitbucket.org/fpgafais/hpcg_fpga).
 * Copyright (c) 2020 G. Korcyl, P. Korcyl
 * 
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU General Public License as published by  
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * 
 * File: su3_matrix.h
 * Authors: G. Korcyl, P. Korcyl
 * Contact: grzegorz.korcyl@uj.edu.pl, piotr.korcyl@uj.edu.pl
 * 
 * Version: 1.0
 * 
 * Description:
 * class definition and linear algebra for 3x3 complex matrices
 * 
 */


#ifndef H_SU3_MATRIX
#define H_SU3_MATRIX

#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <iostream>


#ifndef SOFTONLY
	#include <hls_math.h>
#endif

#include "su3_complex.h"

template <class T>
class su3_matrix {
	public:
		su3_complex<T> m[9];

		su3_matrix() {
			m[0] = 0.0;
			m[1] = 0.0;
			m[2] = 0.0;
			m[3] = 0.0;
			m[4] = 0.0;
			m[5] = 0.0;
			m[6] = 0.0;
			m[7] = 0.0;
			m[8] = 0.0;	
		}

		void print() {
			#ifndef SYNTHESIS
				for (int i = 0; i < 9; i++) {
					std::cerr<<"m["<<i<<"].r = "<<m[i].r<<std::endl;
					std::cerr<<"m["<<i<<"].i = "<<m[i].i<<std::endl;
				}
			#endif
		}

	        void printMatrix(char *cc){
	        	int i,j;

		        for(i = 0; i < 3; i++){
        		        for(j = 0; j < 3; j++){
                		        if(m[i*3+j].r*m[i*3+j].r+m[i*3+j].i*m[i*3+j].i > 1e-6){
                                	        printf("m(%s)[%i] = %1.4e %1.4e\n", cc, i*3+j, m[i*3+j].r, m[i*3+j].i);
	                                }
        	                }
                	}
	        }


		void toVector(T* r) {
			for (int i = 0; i < 9; i++) {
				#pragma HLS unroll
				r[2 * i] = m[i].r;
				r[2 * i + 1] = m[i].i;
			}
		}

		void setZero(void){

			int i;

			for(i=0;i<9;i++){
				m[i].r = 0.0;
				m[i].i = 0.0;
			}
		}

		void fromVector(T* r) {
		// #pragma inline
			m[0].r = r[0];
			m[0].i = r[1];
			m[1].r = r[2];
			m[1].i = r[3];
			m[2].r = r[4];
			m[2].i = r[5];
			m[3].r = r[6];
			m[3].i = r[7];
			m[4].r = r[8];
			m[4].i = r[9];
			m[5].r = r[10];
			m[5].i = r[11];
			m[6].r = r[12];
			m[6].i = r[13];
			m[7].r = r[14];
			m[7].i = r[15];
			m[8].r = r[16];
			m[8].i = r[17];
		}

};

#endif
