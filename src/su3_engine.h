/* 
 * This file is part of the HPCG_FPGA distribution (https://bitbucket.org/fpgafais/hpcg_fpga).
 * Copyright (c) 2020 G. Korcyl, P. Korcyl
 * 
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU General Public License as published by  
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * 
 * File: su3_engine.h
 * Authors: G. Korcyl, P. Korcyl
 * Contact: grzegorz.korcyl@uj.edu.pl, piotr.korcyl@uj.edu.pl
 * 
 * Version: 1.0
 * 
 * Description:
 * class for handling openCL calls
 * 
 */

#ifndef _H_SU3_ENGINE_
#define _H_SU3_ENGINE_

#include "host_utils.h"

#ifdef SOFTONLY
        #include "function1.h"
#endif

#include "fpga_engine.h"
#include "su3_spinor.h"

#include "configs.h"

template <class T_low, class T_high>
class su3_engine {
    public:
        char* input_file;

        su3_matrix<T_high> **U;

        su3_spinor<T_high> *x;
        su3_spinor<T_high> *b;
        su3_spinor<T_high> *r;
        su3_spinor<T_high> *p;
        su3_spinor<T_high> *tmp;

        su3_spinor<T_low> *x_low;
        su3_spinor<T_low> *b_low;
        su3_spinor<T_low> *r_low;
        su3_spinor<T_low> *p_low;
        su3_spinor<T_low> *p_low_old;
        su3_spinor<T_low> *tmp_low;


	//lower part
        su3_matrix<T_high> *U_l_spatial;
        su3_matrix<T_high> *U_l_temporal;
        su3_spinor<T_high> *x_l;
        su3_spinor<T_high> *r_l;

        //upper part
        su3_matrix<T_high> *U_u_spatial;
        su3_matrix<T_high> *U_u_temporal;
        su3_spinor<T_high> *x_u;
        su3_spinor<T_high> *r_u;

        //lower part
        su3_matrix<T_low> *U_l_spatial_low;
        su3_matrix<T_low> *U_l_temporal_low;
        su3_spinor<T_low> *x_l_low;
        su3_spinor<T_low> *r_l_low;

        //upper part
        su3_matrix<T_low> *U_u_spatial_low;
        su3_matrix<T_low> *U_u_temporal_low;
        su3_spinor<T_low> *x_u_low;
        su3_spinor<T_low> *r_u_low;

        T_low* buf0_low;
        T_low* buf1_low;
        T_low* buf2_low;
        T_low* buf3_low;

        T_high* buf0_high;
        T_high* buf1_high;
        T_high* buf2_high;
        T_high* buf3_high;


        su3_engine(char* s) {

                input_file = s;

            buf0_low = new T_low[WORDS_IN_DATASETS2];
            buf1_low = new T_low[WORDS_IN_DATASETS2];
            buf2_low = new T_low[WORDS_IN_DATASETS2];
            //buf3_low = new T_low[WORDS_IN_DATASETS2];

            buf0_high = new T_high[WORDS_IN_DATASETS2];
            buf1_high = new T_high[WORDS_IN_DATASETS2];
            buf2_high = new T_high[WORDS_IN_DATASETS2];
            //buf3_high = new T_high[WORDS_IN_DATASETS2];

            for (int i = 0; i < WORDS_IN_DATASETS2; i++) {
            	buf0_low[i] = 0;
            	buf1_low[i] = 0;
            	buf2_low[i] = 0;
            	//buf3_low[i] = 0;
            	buf0_high[i] = 0;
            	buf1_high[i] = 0;
            	buf2_high[i] = 0;
            	//buf3_high[i] = 0;
            }


            U = new su3_matrix<T_high>*[Vv];
            for(int i = 0; i < Vv; i++)
                U[i] = new su3_matrix<T_high>[4];

            std::cout<<"U allocated"<<std::endl;

            x = new su3_spinor<T_high>[Vv];
            b = new su3_spinor<T_high>[Vv];
            r = new su3_spinor<T_high>[Vv];
            p = new su3_spinor<T_high>[Vv];
            tmp = new su3_spinor<T_high>[Vv];

            x_low = new su3_spinor<T_low>[Vv];
            b_low = new su3_spinor<T_low>[Vv];
            r_low = new su3_spinor<T_low>[Vv];
            p_low = new su3_spinor<T_low>[Vv];
            p_low_old = new su3_spinor<T_low>[Vv];
            tmp_low = new su3_spinor<T_low>[Vv];

//	    std::cerr<<"X, B, R allocated"<<std::endl;

            std::cout<<"Reading input data file"<<std::endl;

            set_initial_U<T_high>(U,'r', input_file);
            std::cout<<"U initialized"<<std::endl;

            set_initial_b<T_high>(x);
            std::cout<<"X initialized"<<std::endl;

            set_initial_b<T_high>(b);
            std::cout<<"B initialized"<<std::endl;


        //==============================================================================================================
        // I stage of decomposition
        //==============================================================================================================

            std::cout<<"Decomposing into spatial/temporal parts, reducing matrix"<<std::endl;

            //lower part
            U_l_spatial = new su3_matrix<T_high>[3*Vspatial_half_gauge];
            U_l_temporal = new su3_matrix<T_high>[Vtemporal_half_gauge];
            x_l = new su3_spinor<T_high>[Vhalf_x];
            r_l = new su3_spinor<T_high>[Vhalf_x];

            std::cout<<"Lower part allocated"<<std::endl;

            //upper part
            U_u_spatial = new su3_matrix<T_high>[3*Vspatial_half_gauge];
            U_u_temporal = new su3_matrix<T_high>[Vtemporal_half_gauge];
            x_u = new su3_spinor<T_high>[Vhalf_x];
            r_u = new su3_spinor<T_high>[Vhalf_x];

            std::cout<<"Upper part allocated"<<std::endl;

            //lower part
            U_l_spatial_low = new su3_matrix<T_low>[3*Vspatial_half_gauge];
            U_l_temporal_low = new su3_matrix<T_low>[Vtemporal_half_gauge];
            x_l_low = new su3_spinor<T_low>[Vhalf_x];
            r_l_low = new su3_spinor<T_low>[Vhalf_x];

            std::cout<<"Lower part low precision allocated"<<std::endl;

            //upper part
            U_u_spatial_low = new su3_matrix<T_low>[3*Vspatial_half_gauge];
            U_u_temporal_low = new su3_matrix<T_low>[Vtemporal_half_gauge];
            x_u_low = new su3_spinor<T_low>[Vhalf_x];
            r_u_low = new su3_spinor<T_low>[Vhalf_x];

            std::cout<<"Upper part low precision allocated"<<std::endl;
            std::cout<<"su3 object created"<<std::endl;

        }

        ~su3_engine() {
        	delete[] buf0_low;
        	delete[] buf1_low;
        	delete[] buf2_low;
        	//delete[] buf3_low;

        	delete[] buf0_high;
        	delete[] buf1_high;
        	delete[] buf2_high;
        	//delete[] buf3_high;


			for(int i = 0; i < Vv; i++)
				delete[] U[i];

			delete[] U;

			delete[] x;
			delete[] b;
			delete[] r;
			delete[] p;
			delete[] tmp;

			delete[] x_low;
			delete[] b_low;
			delete[] r_low;
			delete[] p_low;
			delete[] p_low_old;
			delete[] tmp_low;

			delete[] U_l_spatial;
			delete[] U_l_temporal;
			delete[] x_l;
			delete[] r_l;

			delete[] U_u_spatial;
			delete[] U_u_temporal;
			delete[] x_u;
			delete[] r_u;

			delete[] U_l_spatial_low;
			delete[] U_l_temporal_low;
			delete[] x_l_low;
			delete[] r_l_low;

			delete[] U_u_spatial_low;
			delete[] U_u_temporal_low;
			delete[] x_u_low;
			delete[] r_u_low;
        }

        void initialize() {

            int i, ii, iii, xx, yy, zz, tt, dir;

//            std::cerr<<"initializing U_l_temporal"<<std::endl;

            //dir = 0 => t, dir = 1 => zz; ...
            for(dir = 0; dir < 4; dir++) {
                    for(tt = 0; tt < Tt; tt++){
                            for(zz = 0; zz < Nn; zz++){
                                    for(yy = 0; yy < Nn; yy++){
                                            for(xx = 0; xx < Nn; xx++){

                                                    i = tt*Nn*Nn*Nn + zz*Nn*Nn + yy*Nn + xx;

                                                    if(dir == 0){

                                                            if (tt < Tt / 2 - 1) {
                                                                    U_l_temporal[i+Nn*Nn*Nn] = U[i][dir];
                                                            }
                                                            else if (tt == Tt / 2 - 1) {
                                                                    U_l_temporal[i+Nn*Nn*Nn] = U[i][dir];

                                                                    U_u_temporal[i-(Tt/2-1)*Nn*Nn*Nn] = U[i][dir];
                                                            }
                                                            else if (tt >= Tt / 2 && tt < Tt - 1) {
                                                                    U_u_temporal[i-(Tt/2-1)*Nn*Nn*Nn] = U[i][dir];
                                                            }
                                                            //periodic boundary conditions
                                                            else if (tt == Tt - 1) {
                                                                    U_u_temporal[i-(Tt/2-1)*Nn*Nn*Nn] = U[i][dir];

                                                                    U_l_temporal[i-(Tt-1)*Nn*Nn*Nn] = U[i][dir];
                                                            }
                                                    } //if
                                                    if( dir > 0 ){

                                                            if (tt < Tt / 2 ) {
                                                                    U_l_spatial[i + (dir-1)*Tt*Nn*Nn*Nn - (dir-1)*(Tt/2*Nn*Nn*Nn)] = U[i][dir];
                                                            }
                                                            else if (tt >= Tt / 2 && tt < Tt) {
                                                                    U_u_spatial[i + (dir-1)*Tt*Nn*Nn*Nn - dir*(Tt/2)*Nn*Nn*Nn] = U[i][dir];
                                                            }

                                                    } //if
                                            } //xx
                                    } //yy
                            } //zz
                    } //tt
            } //dir


            for(tt = 0; tt < Tt; tt++){
                    for(zz = 0; zz < Nn; zz++){
                            for(yy = 0; yy < Nn; yy++){
                                    for(xx = 0; xx < Nn; xx++){

                                            i = tt*Nn*Nn*Nn + zz*Nn*Nn + yy*Nn + xx;

                                            for (ii = 0; ii < 4; ii++) {
                                                    for (iii = 0; iii < 3; iii++) {

                                                            if ( tt == 0) {
                                                                    x_l[i+Nn*Nn*Nn].s[ii].v[iii] = x[i].s[ii].v[iii];

                                                                    x_u[i+(Tt/2+1)*Nn*Nn*Nn].s[ii].v[iii] = x_l[i+Nn*Nn*Nn].s[ii].v[iii];
                                                            }
                                                            else if ( tt > 0 && tt < Tt / 2 - 1) {
                                                                    x_l[i+Nn*Nn*Nn].s[ii].v[iii] = x[i].s[ii].v[iii];
                                                            }
                                                            else if ( tt >= Tt / 2 - 1 && tt <= Tt / 2 ) {
                                                                    x_l[i+Nn*Nn*Nn].s[ii].v[iii] = x[i].s[ii].v[iii];

                                                                    x_u[i-(Tt/2-1)*Nn*Nn*Nn].s[ii].v[iii] = x_l[i+Nn*Nn*Nn].s[ii].v[iii];
                                                            }
                                                            else if ( tt > Tt / 2  && tt < Tt - 1) {
                                                                    x_u[i-(Tt/2-1)*Nn*Nn*Nn].s[ii].v[iii] = x[i].s[ii].v[iii];
                                                            }
                                                            else if ( tt == Tt - 1) {
                                                                    x_u[i-(Tt/2-1)*Nn*Nn*Nn].s[ii].v[iii] = x[i].s[ii].v[iii];

                                                                    x_l[i-(Tt-1)*Nn*Nn*Nn].s[ii].v[iii] = x_u[i-(Tt/2-1)*Nn*Nn*Nn].s[ii].v[iii];
                                }
                                                    }
                                            }
                                    }
                            }
                    }
            }


//            std::cerr<<"reducing high to low type of U_temporal"<<std::endl;

            for(i = 0; i < Vtemporal_half_gauge; i++) {
                for(iii = 0; iii < 18; iii++) {

                    U_l_temporal_low[i].m[iii].r = static_cast<T_low>(U_l_temporal[i].m[iii].r);
                    U_l_temporal_low[i].m[iii].i = static_cast<T_low>(U_l_temporal[i].m[iii].i);

                    U_u_temporal_low[i].m[iii].r = static_cast<T_low>(U_u_temporal[i].m[iii].r);
                    U_u_temporal_low[i].m[iii].i = static_cast<T_low>(U_u_temporal[i].m[iii].i);

                }
            }

//            std::cerr<<"reducing high to low type of U_spatial"<<std::endl;

            for(i = 0; i < 3*Vspatial_half_gauge; i++) {
                for(iii = 0; iii < 18; iii++) {

                    U_l_spatial_low[i].m[iii].r = static_cast<T_low>(U_l_spatial[i].m[iii].r);
                    U_l_spatial_low[i].m[iii].i = static_cast<T_low>(U_l_spatial[i].m[iii].i);

                    U_u_spatial_low[i].m[iii].r = static_cast<T_low>(U_u_spatial[i].m[iii].r);
                    U_u_spatial_low[i].m[iii].i = static_cast<T_low>(U_u_spatial[i].m[iii].i);
                }
            }


//            std::cerr<<"reducing high to low type of x"<<std::endl;


            for(i = 0; i < Vhalf_x; i++) {
                for(ii = 0; ii < 4; ii++) {
                    for(iii = 0; iii < 3; iii++) {

                        x_l_low[i].s[ii].v[iii].r = static_cast<T_low>(x_l[i].s[ii].v[iii].r);
                        x_l_low[i].s[ii].v[iii].i = static_cast<T_low>(x_l[i].s[ii].v[iii].i);

                            r_l[i].s[ii].v[iii] = 0;
                        r_l_low[i].s[ii].v[iii] = 0;

                        x_u_low[i].s[ii].v[iii].r = static_cast<T_low>(x_u[i].s[ii].v[iii].r);
                        x_u_low[i].s[ii].v[iii].i = static_cast<T_low>(x_u[i].s[ii].v[iii].i);

                            r_u[i].s[ii].v[iii] = 0;
                        r_u_low[i].s[ii].v[iii] = 0;

                    }
                }
            }

            std::cout<<"su3 object initialized"<<std::endl;
	}

int split(su3_spinor<T_high> *x_l, su3_spinor<T_high> *x_u, su3_spinor<T_high> *tmp){

        int tt,zz,yy,xx,ii,iii;

        for(tt = 0; tt < Tt; tt++){
                for(zz = 0; zz < Nn; zz++){
                        for(yy = 0; yy < Nn; yy++){
                                for(xx = 0; xx < Nn; xx++){

                                        int i = tt*Nn*Nn*Nn + zz*Nn*Nn + yy*Nn + xx;
      
                                        for (ii = 0; ii < 4; ii++) {
                                                for (iii = 0; iii < 3; iii++) {
    
                                                        if ( tt == 0) {
                                                                x_l[i+Nn*Nn*Nn].s[ii].v[iii] = tmp[i].s[ii].v[iii];
    
                                                                x_u[i+(Tt/2+1)*Nn*Nn*Nn].s[ii].v[iii] = x_l[i+Nn*Nn*Nn].s[ii].v[iii];
    
                                                        }
                                                        else if ( tt > 0 && tt < Tt / 2 - 1) {
                                                                x_l[i+Nn*Nn*Nn].s[ii].v[iii] = tmp[i].s[ii].v[iii];
                                                        }
                                                        else if ( tt >= Tt / 2 - 1 && tt <= Tt / 2 ) {
                                                                x_l[i+Nn*Nn*Nn].s[ii].v[iii] = tmp[i].s[ii].v[iii];

                                                                x_u[i-(Tt/2-1)*Nn*Nn*Nn].s[ii].v[iii] = x_l[i+Nn*Nn*Nn].s[ii].v[iii];
                                                        }
                                                        else if ( tt > Tt / 2  && tt < Tt - 1) {
                                                                x_u[i-(Tt/2-1)*Nn*Nn*Nn].s[ii].v[iii] = tmp[i].s[ii].v[iii];
                                                        }
                                                        else if ( tt == Tt - 1) {
                                                                x_u[i-(Tt/2-1)*Nn*Nn*Nn].s[ii].v[iii] = tmp[i].s[ii].v[iii];

                                                                x_l[i-(Tt-1)*Nn*Nn*Nn].s[ii].v[iii] = x_u[i-(Tt/2-1)*Nn*Nn*Nn].s[ii].v[iii];
                                                        }
                                                }
                                        }
                                }
                        }
                }
        }

return 1;
}

int gather(su3_spinor<T_high> *tmp, su3_spinor<T_high> *x_l, su3_spinor<T_high> *x_u){

        int tt,zz,yy,xx,ii,iii;

        for(tt = 1; tt <= Tt/2; tt++){
                for(zz = 0; zz < Nn; zz++){
                        for(yy = 0; yy < Nn; yy++){
                                for(xx = 0; xx < Nn; xx++){

                                        int i = tt*Nn*Nn*Nn + zz*Nn*Nn + yy*Nn + xx;

                                        for (ii = 0; ii < 4; ii++){
                                                for (iii = 0; iii < 3; iii++) {
                                                        tmp[(i - Nn*Nn*Nn)].s[ii].v[iii] = x_l[i].s[ii].v[iii];
                                                }
                                        }
                                }
                        }
                }
        }

        for(tt = 1; tt <= Tt/2; tt++){
                for(zz = 0; zz < Nn; zz++){
                        for(yy = 0; yy < Nn; yy++){
                                for(xx = 0; xx < Nn; xx++){

                                        int i = tt*Nn*Nn*Nn + zz*Nn*Nn + yy*Nn + xx;

                                        for (ii = 0; ii < 4; ii++) {
                                                for (iii = 0; iii < 3; iii++) {
                                                        tmp[(i - Nn*Nn*Nn) + Vv/2].s[ii].v[iii] = x_u[i].s[ii].v[iii];
                                                }
                                        }
                                }
                        }
                }
        }

return 1;
}

	void prepare_data_sets_high(std::vector<T_high, aligned_allocator<T_high>>* buf0) {

            multBatch<T_high>(U_l_temporal, U_u_temporal, U_l_spatial, U_u_spatial, x_l, x_u, 'd', buf0_high);

        for (int i = 0; i < WORDS_IN_DATASETS2; i++) {
                (*buf0)[i] = buf0_high[i];
            }
        }

	void prepare_data_sets_high(std::vector<T_high, aligned_allocator<T_high>>* buf0, std::vector<T_high, aligned_allocator<T_high>>* buf1, std::vector<T_high, aligned_allocator<T_high>>* buf2) {

            multBatch<T_high>(U_l_temporal, U_u_temporal, U_l_spatial, U_u_spatial, x_l, x_u, 'd', buf0_high, buf1_high, buf2_high);

        for (int i = 0; i < WORDS_IN_DATASETS2 / 3; i++) {
                (*buf0)[i] = buf0_high[i];
				(*buf1)[i] = buf1_high[i];
				(*buf2)[i] = buf2_high[i];
            }
        }

        void prepare_data_sets_low(std::vector<T_low, aligned_allocator<T_low>>* buf0) {

            multBatch<T_low>(U_l_temporal_low, U_u_temporal_low, U_l_spatial_low, U_u_spatial_low, x_l_low, x_u_low, 'd', buf0_low);

        for (int i = 0; i < WORDS_IN_DATASETS2; i++) {
                (*buf0)[i] = buf0_low[i];
            }
        }

        void prepare_data_sets_low(std::vector<T_low, aligned_allocator<T_low>>* buf0, std::vector<T_low, aligned_allocator<T_low>>* buf1, std::vector<T_low, aligned_allocator<T_low>>* buf2) {

            multBatch<T_low>(U_l_temporal_low, U_u_temporal_low, U_l_spatial_low, U_u_spatial_low, x_l_low, x_u_low, 'd', buf0_low, buf1_low, buf2_low);

			for (int i = 0; i < WORDS_IN_DATASETS2 / 3; i++) {
					(*buf0)[i] = buf0_low[i];
					(*buf1)[i] = buf1_low[i];
					(*buf2)[i] = buf2_low[i];
			}
        }


	void switch_r_to_x(void){

        	int i, ii, iii;

					for(i = 0; i < Vhalf_x; i++){

        	                                for (ii = 0; ii < 4; ii++) {
                	                                for (iii = 0; iii < 3; iii++) {
	
        	                                                x_l[i].s[ii].v[iii] = r_l[i].s[ii].v[iii];
                	                                        x_u[i].s[ii].v[iii] = r_u[i].s[ii].v[iii];
                        	                        }
                                	        }
					}
	}

	void switch_r_to_x_low(void){

        	int i, ii, iii;

					for(i = 0; i < Vhalf_x; i++){

        	                                for (ii = 0; ii < 4; ii++) {
                	                                for (iii = 0; iii < 3; iii++) {
	
        	                                                x_l_low[i].s[ii].v[iii] = r_l_low[i].s[ii].v[iii];
                	                                        x_u_low[i].s[ii].v[iii] = r_u_low[i].s[ii].v[iii];
                        	                        }
                                	        }
					}
	}



	void process_results_high(std::vector<T_high, aligned_allocator<T_high>>* buf0) { //}, std::vector<T_high, aligned_allocator<T_high>>* buf1) {

        	T_high tab[24];

		int zz,yy,xx,ii,iii, i;

		double sum = 0;

		su3_spinor<T_high> result;

		for(i = 2*Nn*Nn*Nn; i < (Tt/2+2)*Nn*Nn*Nn; i++){

			for (ii = 0; ii < 24; ii++){
				tab[ii] = buf0->at( i * 32 + ii );
				sum += tab[ii]*tab[ii];
			}
			reconstructSpinor<T_high>(tab, &result);

			r_l[i - Nn*Nn*Nn] = result;
		}

                for(i = (Tt/2+4)*Nn*Nn*Nn; i < (Tt+4)*Nn*Nn*Nn; i++){

			for (ii = 0; ii < 24; ii++){
				tab[ii] = buf0->at( i * 32 + ii );
				sum += tab[ii]*tab[ii];
			}
			reconstructSpinor<T_high>(tab, &result);

			r_u[i - (Tt/2+3)*Nn*Nn*Nn] = result;
		}

				for(zz = 0; zz < Nn; zz++){
					for(yy = 0; yy < Nn; yy++){
						for(xx = 0; xx < Nn; xx++){

							int i = zz*Nn*Nn + yy*Nn + xx;

							for (ii = 0; ii < 4; ii++) {
								for (iii = 0; iii < 3; iii++) {

										 r_u[i+(Tt/2+1)*Nn*Nn*Nn].s[ii].v[iii] = r_l[i+Nn*Nn*Nn].s[ii].v[iii];
										 r_l[i+(Tt/2+1)*Nn*Nn*Nn].s[ii].v[iii] = r_u[i+Nn*Nn*Nn].s[ii].v[iii];

										 r_u[i].s[ii].v[iii] = r_l[i+(Tt/2)*Nn*Nn*Nn].s[ii].v[iii];
										 r_l[i].s[ii].v[iii] = r_u[i+(Tt/2)*Nn*Nn*Nn].s[ii].v[iii];
								}
							}
						}
					}
				}

        }

        void process_results_low(std::vector<T_low, aligned_allocator<T_low>>* buf0) { //}, std::vector<T_high, aligned_allocator<T_high>>* buf1) {

        	T_low tab[24];

		int zz,yy,xx,ii,iii, i;

		double sum = 0;

		su3_spinor<T_low> result;

		for(i = 2*Nn*Nn*Nn; i < (Tt/2+2)*Nn*Nn*Nn; i++){

			for (ii = 0; ii < 24; ii++){
				tab[ii] = buf0->at( i * 32 + ii );
				sum += tab[ii]*tab[ii];
			}
			reconstructSpinor<T_low>(tab, &result);
			
			r_l_low[i - Nn*Nn*Nn] = result;
		}

                for(i = (Tt/2+4)*Nn*Nn*Nn; i < (Tt+4)*Nn*Nn*Nn; i++){

			for (ii = 0; ii < 24; ii++){
				tab[ii] = buf0->at( i * 32 + ii );
				sum += tab[ii]*tab[ii];
			}
			reconstructSpinor<T_low>(tab, &result);

			r_u_low[i - (Tt/2+3)*Nn*Nn*Nn] = result;
		}

				for(zz = 0; zz < Nn; zz++){
					for(yy = 0; yy < Nn; yy++){
						for(xx = 0; xx < Nn; xx++){

							int i = zz*Nn*Nn + yy*Nn + xx;

							for (ii = 0; ii < 4; ii++) {
								for (iii = 0; iii < 3; iii++) {


										 r_u_low[i+(Tt/2+1)*Nn*Nn*Nn].s[ii].v[iii] = r_l_low[i+Nn*Nn*Nn].s[ii].v[iii];
										 r_l_low[i+(Tt/2+1)*Nn*Nn*Nn].s[ii].v[iii] = r_u_low[i+Nn*Nn*Nn].s[ii].v[iii];

										 r_u_low[i].s[ii].v[iii] = r_l_low[i+(Tt/2)*Nn*Nn*Nn].s[ii].v[iii];
										 r_l_low[i].s[ii].v[iii] = r_u_low[i+(Tt/2)*Nn*Nn*Nn].s[ii].v[iii];
								}
							}
						}
					}
				}

        }




	int gather_high(){

        	int tt,zz,yy,xx,ii,iii;

                for(tt = 1; tt <= Tt/2; tt++){
                        for(zz = 0; zz < Nn; zz++){
                                for(yy = 0; yy < Nn; yy++){
                                        for(xx = 0; xx < Nn; xx++){

                                                int i = tt*Nn*Nn*Nn + zz*Nn*Nn + yy*Nn + xx;

                                                for (ii = 0; ii < 4; ii++){
                                                        for (iii = 0; iii < 3; iii++) {
                                                                x[(i - Nn*Nn*Nn)].s[ii].v[iii] = r_l[i].s[ii].v[iii];
                                                        }
                                                }
                                        }
                                }
                        }
                }

                for(tt = 1; tt <= Tt/2; tt++){
                        for(zz = 0; zz < Nn; zz++){
                                for(yy = 0; yy < Nn; yy++){
                                        for(xx = 0; xx < Nn; xx++){

                                                int i = tt*Nn*Nn*Nn + zz*Nn*Nn + yy*Nn + xx;

                                                for (ii = 0; ii < 4; ii++) {
                                                        for (iii = 0; iii < 3; iii++) {
                                                                x[(i - Nn*Nn*Nn) + Vv/2].s[ii].v[iii] = r_u[i].s[ii].v[iii];
                                                        }
                                                }
                                        }
                                }
                        }
                }

                return 1;
        }

        void calculate_rsold() {

                T_high rsold = 0;

                for(int i = 0; i < Vv; i++){

                        rsold += x[i].times_spinor_to_number(x[i]);
                }

        }

	void run_kernel_high(fpga_engine<T_high,T_low> *engine) {
    	
		prepare_data_sets_high(engine->get_kernel_high(0)->get_out_buffer(0)->dm, engine->get_kernel_high(0)->get_out_buffer(1)->dm, engine->get_kernel_high(0)->get_out_buffer(2)->dm);

		#ifdef SOFTONLY
		function_emu<T_high>(*engine->get_kernel_high(0)->get_out_buffer(0)->dm, *engine->get_kernel_high(0)->get_out_buffer(1)->dm, *engine->get_kernel_high(0)->get_out_buffer(2)->dm, *engine->get_kernel_high(0)->get_in_buffer(0)->dm, "1");
		#endif

		#ifndef SOFTONLY
		engine->enqueue_all_outputs();
		engine->enqueue_kernels();
		engine->execute_queue();

		engine->enqueue_all_inputs();
		engine->execute_queue();
		#endif

    		process_results_high(engine->get_kernel_high(0)->get_in_buffer(0)->dm);
	}

	void run_kernel_low(fpga_engine<T_high,T_low> *engine) {
    
		prepare_data_sets_low(engine->get_kernel_high(0)->get_out_buffer(0)->dm, engine->get_kernel_high(0)->get_out_buffer(1)->dm, engine->get_kernel_high(0)->get_out_buffer(2)->dm);

		#ifdef SOFTONLY
		function_emu<T_high>(*engine->get_kernel_high(0)->get_out_buffer(0)->dm, *engine->get_kernel_high(0)->get_out_buffer(1)->dm, *engine->get_kernel_high(0)->get_out_buffer(2)->dm, *engine->get_kernel_high(0)->get_in_buffer(0)->dm, "1");
		#endif

		#ifndef SOFTONLY
		engine->enqueue_all_outputs();
		engine->enqueue_kernels();
		engine->execute_queue();

		engine->enqueue_all_inputs();
		engine->execute_queue();
		#endif

		process_results_low(engine->get_kernel_high(0)->get_in_buffer(0)->dm);
	}


void do_the_algorithm(fpga_engine<T_high,T_low> *engine){

	
	int iter_max = 1;
	int iter_low_max = 2;

	int iter = 0;

	double residuum = 10e-16;

//==============================================================================================================
//==============================================================================================================
//    ALGORITHM
//==============================================================================================================
//==============================================================================================================

	run_kernel_high(engine);

        switch_r_to_x();

        run_kernel_high(engine);

        gather(tmp, r_l, r_u);

//==============================================================================================================
// high precision initialization
//==============================================================================================================


        T_high rsnew = 0;
        T_high rsold = 0;

	int i, ii, iii;

        for(i = 0; i < Vv; i++){

                r[i] = b[i] - tmp[i];
                p[i] = r[i];

                rsold += tmp[i].times_spinor_to_number( tmp[i] );
        }


//==============================================================================================================
//-------------------------------------------------high precision-------------------------------------------------
//==============================================================================================================


        for(i = 0; i < Vv; i++){
                for(ii = 0; ii < 4; ii++){
                        for(iii = 0; iii < 3; iii++){
                                        p_low[i].s[ii].v[iii].r = static_cast<T_low>(p[i].s[ii].v[iii].r);
                                        p_low[i].s[ii].v[iii].i = static_cast<T_low>(p[i].s[ii].v[iii].i);
                                        p_low_old[i].s[ii].v[iii].r = static_cast<T_low>(p[i].s[ii].v[iii].r);
                                        p_low_old[i].s[ii].v[iii].i = static_cast<T_low>(p[i].s[ii].v[iii].i);

                                        b_low[i].s[ii].v[iii].r = static_cast<T_low>(b[i].s[ii].v[iii].r);
                                        b_low[i].s[ii].v[iii].i = static_cast<T_low>(b[i].s[ii].v[iii].i);
                        }
                }
        }


        T_high rsold_old;

	T_low rsold_low;
	T_low rsnew_low;

        rsold_low = rsold;

	T_low alpha_low;
	T_low beta_low;


        for(iter = 0; iter < iter_max; iter++){

//==============================================================================================================
//-------------------------------------------------low precision initialization-------------------------------------------------
//==============================================================================================================


                std::cerr<<"starting with rsold = "<<rsold<<std::endl;

                for(i = 0; i < Vv; i++){
                        for(ii = 0; ii < 4; ii++){
                                for(iii = 0; iii < 3; iii++){

                                                r_low[i].s[ii].v[iii].r = (1.0/rsold) * static_cast<T_low>(r[i].s[ii].v[iii].r);
                                                r_low[i].s[ii].v[iii].i = (1.0/rsold) * static_cast<T_low>(r[i].s[ii].v[iii].i);

                                                x_low[i].s[ii].v[iii].r = 0.0; 
                                                x_low[i].s[ii].v[iii].i = 0.0; 
                                }
                        }
                }

                T_low proj = 0;

		// rsnew in the first iteration is 0
                if(iter == 0)
                        beta_low = 0.0;
                else
                        beta_low = rsnew/rsold_old/rsold_low;

                rsold_low = 0;

                for(i = 0; i < Vv; i++){


                        proj += r_low[i].times_spinor_to_number( p_low_old[i] );

                        rsold_low += r_low[i].times_spinor_to_number( r_low[i] );

                }

                for(i = 0; i < Vv; i++){
                        for(ii = 0; ii < 4; ii++){
                                for(iii = 0; iii < 3; iii++){
                                                p_low[i].s[ii].v[iii].r = p_low_old[i].s[ii].v[iii].r - proj * r_low[i].s[ii].v[iii].r;
                                                p_low[i].s[ii].v[iii].i = p_low_old[i].s[ii].v[iii].i - proj * r_low[i].s[ii].v[iii].i;
                                }
                        }
                }

                for(i = 0; i < Vv; i++) {
                        for(ii = 0; ii < 4; ii++){
                                for(iii = 0; iii < 3; iii++){
					p_low[i].s[ii].v[iii].r = r_low[i].s[ii].v[iii].r + beta_low * p_low[i].s[ii].v[iii].r;
					p_low[i].s[ii].v[iii].i = r_low[i].s[ii].v[iii].i + beta_low * p_low[i].s[ii].v[iii].i;
				}
			}
                }


//==============================================================================================================
//-------------------------------------------------low precision-------------------------------------------------
//==============================================================================================================

		int iter_low;


                for(iter_low = 0; iter_low < iter_low_max; iter_low++){


			split(x_l_low, x_u_low, p_low);

			run_kernel_low(engine);

		        switch_r_to_x_low();

		        run_kernel_low(engine);

                        gather(tmp_low, r_l_low, r_u_low);


                        T_low pAp_low = 0.0;

                        for(i = 0; i < Vv; i++){
                                pAp_low += p_low[i].times_spinor_to_number( tmp_low[i] );
			}

			alpha_low = rsold_low/pAp_low;

                        rsnew_low = 0;

                        for(i = 0; i < Vv; i++){
	                        for(ii = 0; ii < 4; ii++){
        	                        for(iii = 0; iii < 3; iii++){

						x_low[i].s[ii].v[iii].r = x_low[i].s[ii].v[iii].r + alpha_low * p_low[i].s[ii].v[iii].r;
		                                x_low[i].s[ii].v[iii].i = x_low[i].s[ii].v[iii].i + alpha_low * p_low[i].s[ii].v[iii].i;

                		                r_low[i].s[ii].v[iii].i = r_low[i].s[ii].v[iii].i - alpha_low * tmp_low[i].s[ii].v[iii].i;
	               		                r_low[i].s[ii].v[iii].r = r_low[i].s[ii].v[iii].r - alpha_low * tmp_low[i].s[ii].v[iii].r;
					}
				}

                                rsnew_low += r_low[i].times_spinor_to_number( r_low[i] );
                        }

                        beta_low = rsnew_low / rsold_low;

                        for(i = 0; i < Vv; i++) {
                                for(ii = 0; ii < 4; ii++){
                                        for(iii = 0; iii < 3; iii++){

		                                p_low[i].s[ii].v[iii].r = r_low[i].s[ii].v[iii].r + beta_low * p_low[i].s[ii].v[iii].r;
                                		p_low[i].s[ii].v[iii].i = r_low[i].s[ii].v[iii].i + beta_low * p_low[i].s[ii].v[iii].i;

						p_low_old[i].s[ii].v[iii].r = p_low[i].s[ii].v[iii].r;
						p_low_old[i].s[ii].v[iii].i = p_low[i].s[ii].v[iii].i;
					}
				}

                        }

                        rsold_low = rsnew_low;

		}


//==============================================================================================================
//-------------------------------------------------end low precision-------------------------------------------------
//==============================================================================================================


                for(i = 0; i < Vv; i++) {
                        for(ii = 0; ii < 4; ii++){
                                for(iii = 0; iii < 3; iii++){

                                        x[i].s[ii].v[iii].r = x[i].s[ii].v[iii].r + rsold * static_cast<T_high>(x_low[i].s[ii].v[iii].r + alpha_low * p_low[i].s[ii].v[iii].r);
                                        x[i].s[ii].v[iii].i = x[i].s[ii].v[iii].i + rsold * static_cast<T_high>(x_low[i].s[ii].v[iii].i + alpha_low * p_low[i].s[ii].v[iii].i);
                                }
                        }
                }

                split(x_l, x_u, x);
		
		run_kernel_high(engine);

		switch_r_to_x();

		run_kernel_high(engine);

                gather(tmp, r_l, r_u);

                rsnew = 0;

                for(i = 0; i < Vv; i++){

                        r[i] = b[i] - tmp[i];
                        rsnew += r[i].times_spinor_to_number( r[i] );

                }
                if (rsnew < residuum){
                        break;
                }
                rsold_old = rsold;
                rsold = rsnew;

                std::cerr<<"iter = "<<iter<<" , rsnew = "<<rsnew<<std::endl;

        }

//==========================================================================

        std::cerr<<"FINAL: iter = "<<iter<<" , rsnew = "<<rsnew<<std::endl;

        split(x_l, x_u, x);

        run_kernel_high(engine);

        gather(x, r_l, r_u);

        for(int tt = 0; tt < Tt; tt++ ){

        double wynik = 0;
	int xx, yy, zz;

        for( zz = 0; zz < Nn; zz++ ){
        for( yy = 0; yy < Nn; yy++ ){
        for( xx = 0; xx < Nn; xx++ ){

        i = tt*Nn*Nn*Nn + zz*Nn*Nn + yy*Nn + xx;

              for(ii = 0; ii < 4; ii++) {
                      for(iii = 0; iii < 3; iii++) {
                                wynik = wynik + (x[i].s[ii].v[iii].r)*(x[i].s[ii].v[iii].r) + (x[i].s[ii].v[iii].i)*(x[i].s[ii].v[iii].i);

                      }
              }

        }}}

        fprintf(stderr, "F: %i %i %i %i   %e\n", tt, zz, yy, xx, wynik);

        }

}
};

#endif
