/* 
 * This file is part of the HPCG_FPGA distribution (https://bitbucket.org/fpgafais/hpcg_fpga).
 * Copyright (c) 2020 G. Korcyl, P. Korcyl
 * 
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU General Public License as published by  
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * 
 * File: fpga_kernel.h
 * Authors: G. Korcyl, P. Korcyl
 * Contact: grzegorz.korcyl@uj.edu.pl, piotr.korcyl@uj.edu.pl
 * 
 * Version: 1.0
 * 
 * Description:
 * Class representing computing kernel instatiated in FPGA
 * 
 */

#ifndef __H_FPGA_KERNEL__
#define __H_FPGA_KERNEL__

#include <iostream>
#include <vector>
#include <CL/cl2.hpp>
#include <CL/cl_ext.h>

#include "configs.h"

#define OCL_CHECK(error, call) \
	call; \
	if (error != CL_SUCCESS) { \
		printf("%s:%d Error calling " #call ", error code is: %d\n", \
		__FILE__,__LINE__, error); \
		exit(EXIT_FAILURE); \
	}


/*
*	Customized buffer allocation for 4K boundary alignment
*/
template <typename T>
struct aligned_allocator
{
  using value_type = T;
  T* allocate(std::size_t num)
  {
    void* ptr = nullptr;
    if (posix_memalign(&ptr,4096,num*sizeof(T)))
      throw std::bad_alloc();
    return reinterpret_cast<T*>(ptr);
  }
  void deallocate(T* p, std::size_t num)
  {
    free(p);
  }
};

/*
*	Structure for combining OpenCL buffer for transport 
*	and an aligned vector of data from the DDR by that buffer
*/
template <class T>
struct data_mover {
	cl::Buffer* buffer;
	std::vector<T, aligned_allocator<T>>* dm;
};

template <class T>
class fpga_kernel {
	public:
		int args;
		cl::Kernel kernel;
		cl::Context* context;

		std::vector<data_mover<T>> out_buffers;
		std::vector<data_mover<T>> in_buffers;


	public:
		fpga_kernel(cl::Program* program, cl::Context* context_ptr, std::string function_name);
		~fpga_kernel();
		void add_out_buffer(int size, int xcl_ddr_bank = 0);
		void add_in_buffer(int size);
		void add_char_arg(char arg);
		int get_out_buffers_number() { return out_buffers.size(); }
		int get_in_buffers_number() { return in_buffers.size(); }
		data_mover<T>* get_out_buffer(int id) { return &out_buffers[id]; }
		data_mover<T>* get_in_buffer(int id) { return &in_buffers[id]; }

		cl::Kernel* get_kernel();

};


template <class T>
fpga_kernel<T>::fpga_kernel(cl::Program* program, cl::Context* context_ptr, std::string function_name) {
	context = context_ptr;
	#ifndef SOFTONLY
		cl_int err;
		kernel = cl::Kernel(*program, function_name.c_str(), &err);
		OCL_CHECK(err, err = err);
	#endif
	args = 0;
}

template <class T>
fpga_kernel<T>::~fpga_kernel() {
	typename std::vector<data_mover<T>>::iterator it;
	for (it  = out_buffers.begin(); it != out_buffers.end(); it++) {
		delete (*it).dm;
#ifndef SOFTONLY
		delete (*it).buffer;
#endif
	}
	for (it  = in_buffers.begin(); it != in_buffers.end(); it++) {
		delete (*it).dm;
#ifndef SOFTONLY
		delete (*it).buffer;
#endif
	}
}

template <class T>
void fpga_kernel<T>::add_in_buffer(int size) {

	data_mover<T> mem;
	mem.dm = new std::vector<T, aligned_allocator<T>>(size * 16, 0);

	#ifndef SOFTONLY
		cl_int err = 0;
		mem.buffer = new cl::Buffer(*context, CL_MEM_USE_HOST_PTR | CL_MEM_WRITE_ONLY, size * 16 * sizeof(T), mem.dm->data(), &err);
		OCL_CHECK(err, err = err);
	#endif
	in_buffers.push_back(mem);
	std::cerr<<"Input Buffer for DDR created as arg: "<<args<<std::endl;

	#ifndef SOFTONLY
		cl::Buffer& buf_ref = *mem.buffer;
		OCL_CHECK(err, err = kernel.setArg(args, buf_ref));
	#endif
	args++;
}

template <class T>
void fpga_kernel<T>::add_out_buffer(int size, int xcl_ddr_bank) {

	data_mover<T> mem;
	mem.dm = new std::vector<T, aligned_allocator<T>>(size , 0);

	cl_int err = 0;

#ifndef SOFTONLY
	cl_mem_ext_ptr_t dm_ext;
	dm_ext.flags = xcl_ddr_bank;
	dm_ext.obj = mem.dm->data();
	dm_ext.param = NULL;
	mem.buffer = new cl::Buffer(*context, CL_MEM_USE_HOST_PTR | CL_MEM_READ_ONLY | CL_MEM_EXT_PTR_XILINX, size * sizeof(T), &dm_ext, &err);
#endif

	OCL_CHECK(err, err = err);

	out_buffers.push_back(mem);
	std::cerr<<"Output Buffer for DDR "<<xcl_ddr_bank<<" created as arg: "<<args<<std::endl;

#ifndef SOFTONLY
	cl::Buffer& buf_ref = *mem.buffer;
	OCL_CHECK(err, err = kernel.setArg(args, buf_ref));
#endif
	args++;
}

template <class T>
void fpga_kernel<T>::add_char_arg(char arg) {
	kernel.setArg(args, arg);
	args++;
}

template <class T>
cl::Kernel* fpga_kernel<T>::get_kernel() {
	return &kernel;
}



#endif
