/* 
 * This file is part of the HPCG_FPGA distribution (https://bitbucket.org/fpgafais/hpcg_fpga).
 * Copyright (c) 2020 G. Korcyl, P. Korcyl
 * 
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU General Public License as published by  
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * 
 * File: function1.cpp
 * Authors: G. Korcyl, P. Korcyl
 * Contact: grzegorz.korcyl@uj.edu.pl, piotr.korcyl@uj.edu.pl
 * 
 * Version: 1.0
 * 
 * Description:
 * Wrapper for the call of the computing kernel instatiated in FPGA
 * 
 */



#include "function1.h"

#include "su3_kernel.h"


extern "C" {

	void function1(ap_uint<512>* in1, ap_uint<512>* in2, ap_uint<512>* in3, ap_uint<512>* out) {
		#pragma HLS interface m_axi port=in1 offset=slave bundle=gmem0 num_write_outstanding=1 max_write_burst_length=2
		#pragma HLS interface m_axi port=in2 offset=slave bundle=gmem1 num_write_outstanding=1 max_write_burst_length=2
		#pragma HLS interface m_axi port=in3 offset=slave bundle=gmem2 num_write_outstanding=1 max_write_burst_length=2
		#pragma HLS interface m_axi port=out offset=slave bundle=gmem3 num_read_outstanding=1 max_read_burst_length=2

		#pragma HLS interface s_axilite port=in1 bundle=control
		#pragma HLS interface s_axilite port=in2 bundle=control
		#pragma HLS interface s_axilite port=in3 bundle=control
		#pragma HLS interface s_axilite port=out bundle=control

		#pragma HLS interface s_axilite port=return bundle=control

		function_body<high_type>(in1, in2, in3, out);
	}

	void function2(ap_uint<512>* in1, ap_uint<512>* in2, ap_uint<512>* in3, ap_uint<512>* out) {
		#pragma HLS interface m_axi port=in1 offset=slave bundle=gmem4 num_write_outstanding=1 max_write_burst_length=2
		#pragma HLS interface m_axi port=in2 offset=slave bundle=gmem5 num_write_outstanding=1 max_write_burst_length=2
		#pragma HLS interface m_axi port=in3 offset=slave bundle=gmem6 num_write_outstanding=1 max_write_burst_length=2
		#pragma HLS interface m_axi port=out offset=slave bundle=gmem7 num_read_outstanding=1 max_read_burst_length=2

		#pragma HLS interface s_axilite port=in1 bundle=control
		#pragma HLS interface s_axilite port=in2 bundle=control
		#pragma HLS interface s_axilite port=in3 bundle=control
		#pragma HLS interface s_axilite port=out bundle=control

		#pragma HLS interface s_axilite port=return bundle=control

		function_body<high_type>(in1, in2, in3, out);
	}

	void function3(ap_uint<512>* in1, ap_uint<512>* in2, ap_uint<512>* in3, ap_uint<512>* out) {
		#pragma HLS interface m_axi port=in1 offset=slave bundle=gmem8 num_write_outstanding=1 max_write_burst_length=2
		#pragma HLS interface m_axi port=in2 offset=slave bundle=gmem9 num_write_outstanding=1 max_write_burst_length=2
		#pragma HLS interface m_axi port=in3 offset=slave bundle=gmem10 num_write_outstanding=1 max_write_burst_length=2
		#pragma HLS interface m_axi port=out offset=slave bundle=gmem11 num_read_outstanding=1 max_read_burst_length=2

		#pragma HLS interface s_axilite port=in1 bundle=control
		#pragma HLS interface s_axilite port=in2 bundle=control
		#pragma HLS interface s_axilite port=in3 bundle=control
		#pragma HLS interface s_axilite port=out bundle=control

		#pragma HLS interface s_axilite port=return bundle=control

		function_body<high_type>(in1, in2, in3, out);
	}
}
