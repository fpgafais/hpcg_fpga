/* 
 * This file is part of the HPCG_FPGA distribution (https://bitbucket.org/fpgafais/hpcg_fpga).
 * Copyright (c) 2020 G. Korcyl, P. Korcyl
 * 
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU General Public License as published by  
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * 
 * File: main.cpp
 * Authors: G. Korcyl, P. Korcyl
 * Contact: grzegorz.korcyl@uj.edu.pl, piotr.korcyl@uj.edu.pl
 * 
 * Version: 1.0
 * 
 * Description:
 * Main function of the HPCG_FPGA framework.
 * Objects for the accelerator card and for the computing engine are created and initialized
 * 
 */


#include <iostream>
#include <fstream>
#include <string>
#include <stdlib.h>
#include <unistd.h>

#include "fpga_engine.h"

#include "configs.h"

#include "su3_engine.h"
#include "host_utils.h"


int main(int argc, char* argv[]) {

	fpga_engine<low_type, high_type> engine;

#ifndef SOFTONLY

    if(argc != 3) {
		std::cout << "Usage: " << argv[0] << " <input_file> " << " <xclbin>" <<std::endl;
		return EXIT_FAILURE;
	}

	std::cerr<<"Input data file"<<std::endl;
    std::cerr<<argv[1]<<std::endl;
    std::cerr<<"Locating bin file"<<std::endl;
	std::cerr<<argv[2]<<std::endl;

	std::cerr<<"Initializing device and kernel"<<std::endl;

	engine.init_device();
	engine.load_bin_file(argv[1]);

#else

 	if(argc != 2) {
		std::cout << "Usage: " << argv[0] << " <input_file> "<<std::endl;
		return EXIT_FAILURE;
	}

	std::cerr<<"Input data file"<<std::endl;
	std::cerr<<argv[1]<<std::endl;

#endif

	engine.create_kernel_high("function1");

	std::cerr<<"Preparing primary data structures"<<std::endl;


#ifndef SOFTONLY
	engine.get_kernel_high(0)->add_out_buffer(WORDS_IN_DATASETS2 / 3, 0 | XCL_MEM_TOPOLOGY);
	engine.get_kernel_high(0)->add_out_buffer(WORDS_IN_DATASETS2 / 3, 1 | XCL_MEM_TOPOLOGY);
	engine.get_kernel_high(0)->add_out_buffer(WORDS_IN_DATASETS2 / 3, 2 | XCL_MEM_TOPOLOGY);
#endif

#ifdef SOFTONLY
	engine.get_kernel_high(0)->add_out_buffer(WORDS_IN_DATASETS2 / 3);
	engine.get_kernel_high(0)->add_out_buffer(WORDS_IN_DATASETS2 / 3);
	engine.get_kernel_high(0)->add_out_buffer(WORDS_IN_DATASETS2 / 3);
#endif

	engine.get_kernel_high(0)->add_in_buffer(OUTPUT_SIZE);


	engine.select_active_resolution(0);

	su3_engine<low_type, high_type> su3(argv[1]);
	std::cerr<<"su3.initialize()!"<<std::endl;
	su3.initialize();
	std::cerr<<"su3.initialized"<<std::endl;

	su3.do_the_algorithm(&engine);

	delete engine.get_kernel_high(0);


	return 0;
}
