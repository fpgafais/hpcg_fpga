/* 
 * This file is part of the HPCG_FPGA distribution (https://bitbucket.org/fpgafais/hpcg_fpga).
 * Copyright (c) 2020 G. Korcyl, P. Korcyl
 * 
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU General Public License as published by  
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * 
 * File: su3_kernel.h
 * Authors: G. Korcyl, P. Korcyl
 * Contact: grzegorz.korcyl@uj.edu.pl, piotr.korcyl@uj.edu.pl
 * 
 * Version: 1.0
 * 
 * Description:
 * implementation of the Dirac-Wilson operator
 * 
 */



#ifndef H_SU3_KERNEL_
#define H_SU3_KERNEL_

#include <ap_int.h>
#include <hls_stream.h>
#include "kernel_utils.h"

#ifndef __SYNTHESIS__
	#include <iostream>
	#include <fstream>
	#include <string>
#endif

#include "neighbours/su3_neighbour_bufer_x_forward.h"
#include "neighbours/su3_neighbour_bufer_x_backward.h"
#include "neighbours/su3_neighbour_bufer_y_forward.h"
#include "neighbours/su3_neighbour_bufer_y_backward.h"
#include "neighbours/su3_neighbour_bufer_z_forward.h"
#include "neighbours/su3_neighbour_bufer_z_backward.h"


template<class T>
su3_spinor<T> multiply_by_A(su3_spinor<T> xx, su3_matrix<T> U[8], su3_spinor<T> x[8]) {
	#pragma HLS inline

	su3_spinor<T> result[1];
	#pragma HLS ARRAY_PARTITION variable=result complete
	#pragma HLS ARRAY_PARTITION variable=result[0].s complete

	su3_vector<T> u[8];
	#pragma HLS ARRAY_PARTITION complete variable=u dim=1

	su3_vector<T> w[8];
	#pragma HLS ARRAY_PARTITION complete variable=w dim=1

	su3_vector<T> uu[8];
	#pragma HLS array_partition variable=uu complete dim=1
	
	su3_vector<T> ww[8];
	#pragma HLS array_partition variable=ww complete dim=1

	for (int i = 0; i < 8; i++) {
		#pragma HLS array_partition variable=u[i].v complete
		#pragma HLS array_partition variable=w[i].v complete
		#pragma HLS array_partition variable=uu[i].v complete
		#pragma HLS array_partition variable=ww[i].v complete
	}

//	result[0].setZero();

	// stage II.a
	su3_vector<T> a1, a2, a3, a4, a5, a6, a7, a8;

	// i * x[0].s[3]
	//backward
	//x
	a1 = !x[0].s[3];
	a2 = !x[0].s[2];
//	//z
	a3 = !x[2].s[2];
	a4 = !x[2].s[3];
//
//	//forward
//	//x
	a5 = !x[4].s[3];
	a6 = !x[4].s[2];
//	//z
	a7 = !x[6].s[2];
	a8 = !x[6].s[3];
//
//	// stage II
//	//backward (1+gamma)
//	//-x
	u[0] = x[0].s[0] + a1;
	w[0] = x[0].s[1] + a2;
//	//-y
	u[1] = x[1].s[0] - x[1].s[3];
	w[1] = x[1].s[1] + x[1].s[2];
//	//-z
	u[2] = x[2].s[0] + a3;
	w[2] = x[2].s[1] - a4;
//	//-t
	u[3] = x[3].s[0] + x[3].s[2];
	w[3] = x[3].s[1] + x[3].s[3];
//
//	//forward (1-gamma)
//	//x
	u[4] = x[4].s[0] - a5;
	w[4] = x[4].s[1] - a6;
//	//y
	u[5] = x[5].s[0] + x[5].s[3];
	w[5] = x[5].s[1] - x[5].s[2];
//	//z
	u[6] = x[6].s[0] - a7;
	w[6] = x[6].s[1] + a8;
//	//t
	u[7] = x[7].s[0] - x[7].s[2];
	w[7] = x[7].s[1] - x[7].s[3];
//
//
	// stage III
	for (int i = 0; i < 4; i++) {
//#pragma HLS unroll factor=2
		dagger_matrix_times_vector<T>(U[i], u[i], w[i], uu[i], ww[i]);
	}
	for (int i = 4; i < 8; i++) {
//#pragma HLS unroll factor=2
		matrix_times_vector<T>(U[i], u[i], w[i], uu[i], ww[i]);
	}
//
//
//
	su3_vector<T> pa1, pa2, pa3, pa4, pa5, pa6;
	su3_vector<T> qa1, qa2, qa3, qa4, qa5, qa6;
	su3_vector<T> ta1, ta2, ta3, ta4, ta5, ta6;
	su3_vector<T> sa1, sa2, sa3, sa4, sa5, sa6;
//
//	// stage IV.a
//	//-x
	a1 = !ww[0];
	a2 = !uu[0];
//	//-z
	a3 = !uu[2];
	a4 = !ww[2];
//
//	//x
	a5 = !ww[4];
	a6 = !uu[4];
//	//z
	a7 = !uu[6];
	a8 = !ww[6];
//
//	// stage IV
//	//-x and -y
	pa1 = uu[0] + uu[1];
//	//-z and -t
	pa2 = uu[2] + uu[3];
//	//x and y
	pa3 = uu[4] + uu[5];
//	//z and t
	pa4 = uu[6] + uu[7];
//
	pa5 = pa1 + pa2;
	pa6 = pa3 + pa4;
	result[0].s[0] = pa5 + pa6;

//	//-x and -y
	qa1 = ww[0] + ww[1];
//	//-z and -t
	qa2 = ww[2] + ww[3];
//	//x and y
	qa3 = ww[4] + ww[5];
//	//z and t
	qa4 = ww[6] + ww[7];
//
	qa5 = qa1 + qa2;
	qa6 = qa3 + qa4;
	result[0].s[1] = qa5 + qa6;

//	//-x and -z
	ta1 = a1 + a3;
//	//y and t
	ta2 = ww[5] + uu[7];
//	//-y and -t
	ta3 = ww[1] + uu[3];
//	//x and z
	ta4 = a5 + a7;
	ta5 = ta1 + ta2;
	ta6 = ta3 + ta4;
	result[0].s[2] = ta6 - ta5;

//	//-x and -y
	sa1 = uu[1] + a2;
//	//z and t
	sa2 = a8 + ww[7];
//	//-t and -z
	sa3 = a4 + ww[3];
//	//x and y
	sa4 = uu[5] + a6;
	sa5 = sa1 + sa2;
	sa6 = sa3 + sa4;
	result[0].s[3] = sa6 - sa5;


	su3_spinor<T> rr[1];
	rr[0] = result[0] + xx;

	return rr[0];
}


template <class T>
double compute(ap_uint<512>* c_in1,  ap_uint<512>* c_in2,  ap_uint<512>* c_in3,  ap_uint<512>* c_out, int num_iterations) {
	#pragma HLS pipeline II=c_cycles_per_iter

	ap_uint<512> a_local1[CYCLES_PER_ITER];
	ap_uint<512> a_local2[CYCLES_PER_ITER];
	ap_uint<512> a_local3[CYCLES_PER_ITER];
	ap_uint<512> a_local4[CYCLES_PER_ITER];
	#pragma HLS array_partition variable=a_local1 complete
	#pragma HLS array_partition variable=a_local2 complete
	#pragma HLS array_partition variable=a_local3 complete
	#pragma HLS array_partition variable=a_local4 complete

	su3_spinor<T> x;
	su3_matrix<T> m[4];
	#pragma HLS array_partition variable=m complete
	#pragma HLS array_partition variable=m[0].m complete
	#pragma HLS array_partition variable=m[1].m complete
	#pragma HLS array_partition variable=m[2].m complete
	#pragma HLS array_partition variable=m[3].m complete

	su3_spinor<T> res;
	T res_d[24];
	#pragma HLS array_partition variable=res_d complete

	static su3_matrix<T> cyclic_buffer_U_spatial_a[2][Nn*Nn*Nn];
	static su3_matrix<T> cyclic_buffer_U_spatial_b[2][Nn*Nn*Nn];
	static su3_matrix<T> cyclic_buffer_U_spatial_c[2][Nn*Nn*Nn];
	static su3_matrix<T> cyclic_buffer_U_temporal[2][Nn*Nn*Nn];
	static su3_spinor<T> cyclic_buffer_x_a[4][Nn*Nn*Nn];
	static su3_spinor<T> cyclic_buffer_x_b[4][Nn*Nn*Nn];
	static su3_spinor<T> cyclic_buffer_x_c[4][Nn*Nn*Nn];

	#pragma HLS array_partition variable=cyclic_buffer_x_a dim=1 complete
	#pragma HLS array_partition variable=cyclic_buffer_x_b dim=1 complete
	#pragma HLS array_partition variable=cyclic_buffer_x_c dim=1 complete


	#pragma HLS data_pack variable=cyclic_buffers_U_spatial_a
	#pragma HLS data_pack variable=cyclic_buffers_U_spatial_b
	#pragma HLS data_pack variable=cyclic_buffers_U_spatial_c

	#pragma HLS data_pack variable=cyclic_buffer_U_temporal

	#pragma HLS resource variable=cyclic_buffer_U_spatial_a core=RAM_T2P_URAM latency=1
	#pragma HLS resource variable=cyclic_buffer_U_spatial_b core=RAM_T2P_URAM latency=1
	#pragma HLS resource variable=cyclic_buffer_U_spatial_c core=RAM_T2P_URAM latency=1

	#pragma HLS resource variable=cyclic_buffer_U_temporal core=RAM_T2P_URAM latency=1

	#pragma HLS resource variable=x_backward_neighbour core=RAM_T2P_URAM latency=1
	#pragma HLS resource variable=y_backward_neighbour core=RAM_T2P_URAM latency=1
	#pragma HLS resource variable=z_backward_neighbour core=RAM_T2P_URAM latency=1
	#pragma HLS resource variable=x_forward_neighbour core=RAM_T2P_URAM latency=1
	#pragma HLS resource variable=y_forward_neighbour core=RAM_T2P_URAM latency=1
	#pragma HLS resource variable=z_forward_neighbour core=RAM_T2P_URAM latency=1


	ap_uint<512> res_vec[2];
	#pragma HLS array_partition variable=res_vec complete

	static int tt = 0;
	static int ii = 0;

	int num_of_iterations = num_iterations;

	c_out[0] = 0;
	c_out[1] = 0;

	tt = (num_of_iterations / (Nn * Nn * Nn)) + 1;
	ii = num_of_iterations % (Nn * Nn * Nn);

	for (int i = 0; i < CYCLES_PER_ITER; i++) {
		#pragma HLS unroll
		a_local1[i] = c_in1[i];
		a_local2[i] = c_in2[i];
		a_local3[i] = c_in3[i];
		a_local4[i] = c_in3[i];
	}


	reconstructMatrixBuffers<T>(&(a_local1[0]), &(a_local2[0]), &(a_local3[0]),m);
	reconstructSpinorBuffers<T>(&(a_local4[0]), &x); //(&(a_local1[4]), &x);


	su3_spinor<T> x_in = x;
	su3_matrix<T> m_in[8];
	#pragma HLS array_partition variable=m_in complete
	su3_spinor<T> xx_in[8];
	#pragma HLS array_partition variable=xx_in complete
	for (int i = 0; i < 8; i++) {
		#pragma HLS array_partition variable=m_in[i].m complete
		#pragma HLS array_partition variable=xx_in[i].s complete
	}

	double sum = 0.0;

	if( (num_of_iterations > 2*(Nn * Nn * Nn) - 1 && num_of_iterations < (Tt/2+2)*Nn*Nn*Nn) ||
	    (num_of_iterations > (Tt/2+4)*(Nn * Nn * Nn) - 1 && num_of_iterations < 2*(Tt/2+2)*Nn*Nn*Nn) ){


		m_in[0] = cyclic_buffer_U_spatial_a[(tt+0)%2][x_backward_neighbour[ii]];
		m_in[1] = cyclic_buffer_U_spatial_b[(tt+0)%2][y_backward_neighbour[ii] ];
		m_in[2] = cyclic_buffer_U_spatial_c[(tt+0)%2][z_backward_neighbour[ii] ];
		m_in[3] = cyclic_buffer_U_temporal[(tt+0)%2][ii];
		m_in[4] = cyclic_buffer_U_spatial_a[(tt+0)%2][ii];
		m_in[5] = cyclic_buffer_U_spatial_b[(tt+0)%2][ii];
		m_in[6] = cyclic_buffer_U_spatial_c[(tt+0)%2][ii];
		m_in[7] = cyclic_buffer_U_temporal[(tt+1)%2][ii];

		if (tt % 3 == 0) {
			xx_in[0] = cyclic_buffer_x_a[0][x_backward_neighbour[ii]];
			xx_in[1] = cyclic_buffer_x_a[0][y_backward_neighbour[ii]];
			xx_in[2] = cyclic_buffer_x_a[1][z_backward_neighbour[ii]];
			xx_in[3] = cyclic_buffer_x_c[0][ii];
			xx_in[4] = cyclic_buffer_x_a[1][x_forward_neighbour[ii]];
			xx_in[5] = cyclic_buffer_x_a[2][y_forward_neighbour[ii]];
			xx_in[6] = cyclic_buffer_x_a[2][z_forward_neighbour[ii]];
			xx_in[7] = x_in; 

			res = multiply_by_A(cyclic_buffer_x_a[3][ii], m_in, xx_in);
		}
		else if(tt % 3 == 1) {
			xx_in[0] = cyclic_buffer_x_b[0][x_backward_neighbour[ii]];
			xx_in[1] = cyclic_buffer_x_b[0][y_backward_neighbour[ii]];
			xx_in[2] = cyclic_buffer_x_b[1][z_backward_neighbour[ii]];
			xx_in[3] = cyclic_buffer_x_a[0][ii];
			xx_in[4] = cyclic_buffer_x_b[1][x_forward_neighbour[ii]];
			xx_in[5] = cyclic_buffer_x_b[2][y_forward_neighbour[ii]];
			xx_in[6] = cyclic_buffer_x_b[2][z_forward_neighbour[ii]];
			xx_in[7] = x_in; 

			res = multiply_by_A(cyclic_buffer_x_b[3][ii], m_in, xx_in);
		}
		else if (tt % 3 == 2) {
			xx_in[0] = cyclic_buffer_x_c[0][x_backward_neighbour[ii]];
			xx_in[1] = cyclic_buffer_x_c[0][y_backward_neighbour[ii]];
			xx_in[2] = cyclic_buffer_x_c[1][z_backward_neighbour[ii]];
			xx_in[3] = cyclic_buffer_x_b[0][ii];
			xx_in[4] = cyclic_buffer_x_c[1][x_forward_neighbour[ii]];
			xx_in[5] = cyclic_buffer_x_c[2][y_forward_neighbour[ii]];
			xx_in[6] = cyclic_buffer_x_c[2][z_forward_neighbour[ii]];
			xx_in[7] = x_in;

			res = multiply_by_A(cyclic_buffer_x_c[3][ii], m_in, xx_in);
		}

		flattenSpinor<T>(&res, res_d);

		for (int i = 0; i < 16; i++) {
			#pragma HLS unroll
			union {T f; unsigned long int ui; } u;
			u.f = res_d[i];
			res_vec[ 0 ].range( (32 * (i + 1) - 1), (32 * i)) = u.ui;
		}
		for (int i = 0; i < 8; i++) {
			#pragma HLS unroll
			union {T f; unsigned long int ui; } u;
			u.f = res_d[16 + i];
			res_vec[ 1 ].range( (32 * (i + 1) - 1), (32 * i)) = u.ui;
		}
		for (int i = 8; i < 16; i++) {
			#pragma HLS unroll
			union {T f; unsigned long int ui; } u;
			u.f = 0.0;
			res_vec[ 1 ].range( (32 * (i + 1) - 1), (32 * i)) = u.ui;
		}

	}//end of if num_iteratoions > 2(Nn Nn Nn) - 1


	c_out[0] = res_vec[0];
	c_out[1] = res_vec[1];

	for(int qq=0;qq<9;qq++){

		cyclic_buffer_U_spatial_a[(tt+1)%2][          ii].m[qq].r = m[0].m[qq].r;
		cyclic_buffer_U_spatial_b[(tt+1)%2][   ii].m[qq].r = m[1].m[qq].r;
		cyclic_buffer_U_spatial_c[(tt+1)%2][ ii].m[qq].r = m[2].m[qq].r;

		cyclic_buffer_U_spatial_a[(tt+1)%2][          ii].m[qq].i = m[0].m[qq].i;
		cyclic_buffer_U_spatial_b[(tt+1)%2][   ii].m[qq].i = m[1].m[qq].i;
		cyclic_buffer_U_spatial_c[(tt+1)%2][ ii] .m[qq].i= m[2].m[qq].i;


		cyclic_buffer_U_temporal[(tt+0)%2][         ii].m[qq].r = m[3].m[qq].r;
		cyclic_buffer_U_temporal[(tt+0)%2][         ii].m[qq].i = m[3].m[qq].i;
	}


	for(int q=0;q<4;q++){
		for(int qq=0; qq<3;qq++){

			if (tt % 3 == 0) {
			 	for (int ll = 0; ll < 4; ll++) {
			 		#pragma HLS unroll
					cyclic_buffer_x_b[ll][ii].s[q].v[qq].r = x_in.s[q].v[qq].r;
					cyclic_buffer_x_b[ll][ii].s[q].v[qq].i = x_in.s[q].v[qq].i;
			 	}
			}
			else if (tt % 3 == 1) {
			 	for (int ll = 0; ll < 4; ll++) {
			 		#pragma HLS unroll
					cyclic_buffer_x_c[ll][ii].s[q].v[qq].r = x_in.s[q].v[qq].r;
					cyclic_buffer_x_c[ll][ii].s[q].v[qq].i = x_in.s[q].v[qq].i;
			 	}
			}
			else if (tt % 3 == 2) {
			 	for (int ll = 0; ll < 4; ll++) {
			 		#pragma HLS unroll
					cyclic_buffer_x_a[ll][ii].s[q].v[qq].r = x_in.s[q].v[qq].r;
					cyclic_buffer_x_a[ll][ii].s[q].v[qq].i = x_in.s[q].v[qq].i;
				 }
			}
		}
	}

	return sum;
}

static void read_input(ap_uint<512>* in1, hls::stream< ap_uint<512> > &in1_stream,
		ap_uint<512>* in2, hls::stream< ap_uint<512> > &in2_stream,
		ap_uint<512>* in3, hls::stream< ap_uint<512> > &in3_stream) {
	for (int i = 0; i < (NUMBER_OF_DATASETS + 4*Nn*Nn*Nn) * CYCLES_PER_ITER; i++) {
		#pragma HLS pipeline II=1
		#pragma HLS loop_tripcount min=c_trip_count max=c_trip_count

		in1_stream << in1[i];
		in2_stream << in2[i];
		in3_stream << in3[i];
	}
}

static void write_output(ap_uint<512>* out, hls::stream< ap_uint<512> > &out_stream) {
	for (int i = 0; i < (NUMBER_OF_DATASETS + 4*Nn*Nn*Nn) * CYCLES_PER_ITER; i++) {
			#pragma HLS pipeline II=1
			#pragma HLS loop_tripcount min=c_trip_count max=c_trip_count

			out[i] = out_stream.read();
	}
}

template <class T>
static void compute_if(hls::stream< ap_uint<512> > &in1_stream,
		hls::stream< ap_uint<512> > &in2_stream,
		hls::stream< ap_uint<512> > &in3_stream,
		hls::stream< ap_uint<512> > &out_stream) {

	int num_iterations = 0;

	ap_uint<512> local1[CYCLES_PER_ITER];
	ap_uint<512> local2[CYCLES_PER_ITER];
	ap_uint<512> local3[CYCLES_PER_ITER];
	ap_uint<512> r_local[CYCLES_PER_ITER];
	#pragma HLS array_partition variable=local1 complete
	#pragma HLS array_partition variable=local2 complete
	#pragma HLS array_partition variable=local3 complete
	#pragma HLS array_partition variable=r_local complete

	for (int i = 0; i < (NUMBER_OF_DATASETS + 4*Nn*Nn*Nn); i++) {
		#pragma HLS pipeline II=2
		#pragma HLS loop_tripcount min=c_trip_count_compute max=c_trip_count_compute


		local1[0] = in1_stream.read();
		local2[0] = in2_stream.read();
		local3[0] = in3_stream.read();

		local1[1] = in1_stream.read();
		local2[1] = in2_stream.read();
		local3[1] = in3_stream.read();


		compute<T>(&(local1[0]), &(local2[0]), &(local3[0]), &(r_local[0]), num_iterations);

		num_iterations++;

		out_stream << r_local[0];
		out_stream << r_local[1];

	}
}

template <class T>
inline void function_body(ap_uint<512>* in1, ap_uint<512>* in2, ap_uint<512>* in3, ap_uint<512>* out) {
	#pragma HLS inline

	static hls::stream< ap_uint<512> > in1_stream;
	static hls::stream< ap_uint<512> > in2_stream;
	static hls::stream< ap_uint<512> > in3_stream;
	static hls::stream< ap_uint<512> > out_stream;

#pragma HLS dataflow

	read_input(in1, in1_stream, in2, in2_stream, in3, in3_stream);

	compute_if<T>(in1_stream, in2_stream, in3_stream, out_stream);

	write_output(out, out_stream);

}

#endif
