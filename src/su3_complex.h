/* 
 * This file is part of the HPCG_FPGA distribution (https://bitbucket.org/fpgafais/hpcg_fpga).
 * Copyright (c) 2020 G. Korcyl, P. Korcyl
 * 
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU General Public License as published by  
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * 
 * File: su3_complex.h
 * Authors: G. Korcyl, P. Korcyl
 * Contact: grzegorz.korcyl@uj.edu.pl, piotr.korcyl@uj.edu.pl
 * 
 * Version: 1.0
 * 
 * Description:
 * templated complex class of user defined type
 * 
 */


#ifndef H_SU3_COMPLEX
#define H_SU3_COMPLEX

#include <ap_int.h>

template <class T>
class su3_complex {
	public:
		T r, i;

		su3_complex(T rr, T ii) {
			this->r = rr;
			this->i = ii;
		}

		su3_complex() {
			this->r = 0.0;
			this->i = 0.0;
		}

		su3_complex operator*(T alpha) {
			#pragma HLS inline

			this->r *= alpha;
			this->i *= alpha;
			return *this;
		}

		su3_complex operator=(const T& a) {
			#pragma HLS inline

			this->r = a;
			this->i = 0.0;
			return *this;
		}

		su3_complex operator+(const su3_complex &b) {
			#pragma HLS inline

			su3_complex c;
			c.r = r + b.r;
			c.i = i + b.i;

			return c;
		}

		su3_complex operator-(const su3_complex &b) {
			#pragma HLS inline 
			su3_complex c;
			c.r = r - b.r;
			c.i = i - b.i;

			return c;
		}

		su3_complex operator*(const su3_complex &b) {
			#pragma HLS inline

			su3_complex c;
			c.r = r * b.r - i * b.i;
			c.i = r * b.i + i * b.r;

			return c;
		}

		//complex conjugate b and multiply
		su3_complex operator^(const su3_complex &b) {
			#pragma HLS inline

			su3_complex c;
			c.r = r * b.r + i * b.i;
			c.i = r * b.i - i * b.r;

			return c;
		}

		su3_complex conj(){
			#pragma HLS inline
			
			su3_complex z;
			z.r = r;
			z.i = -i;
			return z;
		}

};

#endif
