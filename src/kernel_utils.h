/* 
 * This file is part of the HPCG_FPGA distribution (https://bitbucket.org/fpgafais/hpcg_fpga).
 * Copyright (c) 2020 G. Korcyl, P. Korcyl
 * 
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU General Public License as published by  
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * 
 * File: kernel_utils.h
 * Authors: G. Korcyl, P. Korcyl
 * Contact: grzegorz.korcyl@uj.edu.pl, piotr.korcyl@uj.edu.pl
 * 
 * Version: 1.0
 * 
 * Description:
 * Helper functions for the data transformation on the FPGA: conversion from double and float types to binary, etc.
 * 
 */



#ifndef H_KERNEL_UTILS
#define H_KERNEL_UTILS

#include "su3_spinor.h"
#include "su3_matrix.h"

const unsigned int c_cycles_per_iter = CYCLES_PER_ITER;
const unsigned int c_trip_count = (NUMBER_OF_DATASETS + 4*Nn*Nn*Nn) * CYCLES_PER_ITER;
const unsigned int c_trip_count_compute = (NUMBER_OF_DATASETS + 4*Nn*Nn*Nn);


template <class T>
void reconstructSpinorBuffers(ap_uint<512>* in, su3_spinor<T>* s) {
	// #pragma HLS inline

	T dd[24];
//	#pragma HLS array_partition variable=dd complete

	for (int ii = 8; ii < 16; ii++) {
		#pragma HLS unroll
		union {T f; unsigned long int ui; } u;
		u.ui = in[0].range( (32 * (ii + 1) - 1), (32 * ii));
		dd[ii - 8] = u.f;
	}
	for (int ii = 0; ii < 16; ii++) {
		#pragma HLS unroll
		union {T f; unsigned long int ui; } u;
		u.ui = in[1].range( (32 * (ii + 1) - 1), (32 * ii));
		dd[8 + ii] = u.f;
	}

	s->s[0].v[0].i = dd[0];
	s->s[0].v[0].r = dd[1];
	s->s[0].v[1].i = dd[2];
	s->s[0].v[1].r = dd[3];
	s->s[0].v[2].i = dd[4];
	s->s[0].v[2].r = dd[5];

	s->s[1].v[0].i = dd[6];
	s->s[1].v[0].r = dd[7];
	s->s[1].v[1].i = dd[8];
	s->s[1].v[1].r = dd[9];
	s->s[1].v[2].i = dd[10];
	s->s[1].v[2].r = dd[11];

	s->s[2].v[0].i = dd[12];
	s->s[2].v[0].r = dd[13];
	s->s[2].v[1].i = dd[14];
	s->s[2].v[1].r = dd[15];
	s->s[2].v[2].i = dd[16];
	s->s[2].v[2].r = dd[17];

	s->s[3].v[0].i = dd[18];
	s->s[3].v[0].r = dd[19];
	s->s[3].v[1].i = dd[20];
	s->s[3].v[1].r = dd[21];
	s->s[3].v[2].i = dd[22];
	s->s[3].v[2].r = dd[23];

}

 template <class T>
 inline void reconstructMatrixBuffers(ap_uint<512>* in1, ap_uint<512>* in2, ap_uint<512>* in3, su3_matrix<T>* m) {
 	// #pragma HLS inline

 	T dd[18];
 	#pragma HLS array_partition variable=dd complete

 // u[0](15-0)
 	for(int i = 0; i < 16; i++) {
 		union {T f; unsigned long int ui; } u;
 		u.ui = in1[0].range( (32 * (i + 1) - 1), (32 * i));
 		dd[i] = u.f;
 	}
 // u[0](17-16)
 	for(int i = 0; i < 2; i++) {
 		union {T f; unsigned long int ui; } u;
 		u.ui = in1[1].range( (32 * (i + 1) - 1), (32 * i));
 		dd[i + 16] = u.f;
 	}

 	m[0].fromVector(dd);

 // u[1](13-0)
 	for(int i = 2; i < 16; i++) {
 		union {T f; unsigned long int ui; } u;
 		u.ui = in1[1].range( (32 * (i + 1) - 1), (32 * i));
 		dd[i - 2] = u.f;
 	}
 // u[1](17-14)
 	for(int i = 0; i < 4; i++) {
 		union {T f; unsigned long int ui; } u;
 		u.ui = in2[0].range( (32 * (i + 1) - 1), (32 * i));
 		dd[i + 14] = u.f;
 	}

 	m[1].fromVector(dd);

 // u[2](11-0)
 	for(int i = 4; i < 16; i++) {
 		union {T f; unsigned long int ui; } u;
 		u.ui = in2[0].range( (32 * (i + 1) - 1), (32 * i));
 		dd[i - 4] = u.f;
 	}
 // u[2](17-12)
 	for(int i = 0; i < 6; i++) {
 		union {T f; unsigned long int ui; } u;
 		u.ui = in2[1].range( (32 * (i + 1) - 1), (32 * i));
 		dd[i + 12] = u.f;
 	}

 	m[2].fromVector(dd);

 // u[3](9-0)
 	for(int i = 6; i < 16; i++) {
 		union {T f; unsigned long int ui; } u;
 		u.ui = in2[1].range( (32 * (i + 1) - 1), (32 * i));
 		dd[i - 6] = u.f;
 	}
 // u[3](17-10)
 	for(int i = 0; i < 8; i++) {
 		union {T f; unsigned long int ui; } u;
 		u.ui = in3[0].range( (32 * (i + 1) - 1), (32 * i));
 		dd[i + 10] = u.f;
 	}

 	m[3].fromVector(dd);

 }

template <class T>
void flattenSpinor(su3_spinor<T>* s, T* r) {
	#pragma HLS pipeline
	// #pragma HLS inline
	s->toVector(r);
}

template <class T>
void dagger_matrix_times_vector(su3_matrix<T> &a, su3_vector<T> &b, su3_vector<T> &bb, su3_vector<T> &c, su3_vector<T> &cc) {
	// #pragma HLS inline

	su3_complex<T> c0, c1, c2, c3, c4, c5, c6, c7, c8;
	su3_complex<T> cc0, cc1, cc2, cc3, cc4, cc5, cc6, cc7, cc8;
	su3_complex<T> tc0, tc3, tc6, tc1, tc4, tc7;
	su3_complex<T> tcc0, tcc3, tcc6, tcc1, tcc4, tcc7;

//  ( 0 1 2 ) ( 0 )
//  ( 3 4 5 ) ( 1 )
//  ( 6 7 8 ) ( 2 )

	// step 1
	c0 = a.m[0]^b.v[0];
	c1 = a.m[3]^b.v[1];
	c2 = a.m[6]^b.v[2];

	c3 = a.m[1]^b.v[0];
	c4 = a.m[4]^b.v[1];
	c5 = a.m[7]^b.v[2];

	c6 = a.m[2]^b.v[0];
	c7 = a.m[5]^b.v[1];
	c8 = a.m[8]^b.v[2];

	cc0 = a.m[0]^bb.v[0];
	cc1 = a.m[3]^bb.v[1];
	cc2 = a.m[6]^bb.v[2];

	cc3 = a.m[1]^bb.v[0];
	cc4 = a.m[4]^bb.v[1];
	cc5 = a.m[7]^bb.v[2];

	cc6 = a.m[2]^bb.v[0];
	cc7 = a.m[5]^bb.v[1];
	cc8 = a.m[8]^bb.v[2];
//
//	// step 2
	tc0 = c0 + c1;
	c.v[0] = tc0 + c2;
	tc3 = c3 + c4;
	c.v[1] = tc3 + c5;
	tc6 = c6 + c7;
	c.v[2] = tc6 + c8;

	tcc0 = cc0 + cc1;
	cc.v[0] = tcc0 + cc2;
	tcc3 = cc3 + cc4;
	cc.v[1] = tcc3 + cc5;
	tcc6 = cc6 + cc7;
	cc.v[2] = tcc6 + cc8;
}



template <class T>
void matrix_times_vector(su3_matrix<T> &a, su3_vector<T> &b, su3_vector<T> &bb, su3_vector<T> &c, su3_vector<T> &cc) {
	// #pragma HLS inline

	su3_complex<T> c0, c1, c2, c3, c4, c5, c6, c7, c8;
	su3_complex<T> cc0, cc1, cc2, cc3, cc4, cc5, cc6, cc7, cc8;
	su3_complex<T> tc0, tc3, tc6, tc1, tc4, tc7;
	su3_complex<T> tcc0, tcc3, tcc6, tcc1, tcc4, tcc7;

//  ( 0 1 2 ) ( 0 )
//  ( 3 4 5 ) ( 1 )
//  ( 6 7 8 ) ( 2 )

	// step 1
	c0 = a.m[0]*b.v[0];
	c1 = a.m[1]*b.v[1];
	c2 = a.m[2]*b.v[2];

	c3 = a.m[3]*b.v[0];
	c4 = a.m[4]*b.v[1];
	c5 = a.m[5]*b.v[2];

	c6 = a.m[6]*b.v[0];
	c7 = a.m[7]*b.v[1];
	c8 = a.m[8]*b.v[2];

	cc0 = a.m[0]*bb.v[0];
	cc1 = a.m[1]*bb.v[1];
	cc2 = a.m[2]*bb.v[2];

	cc3 = a.m[3]*bb.v[0];
	cc4 = a.m[4]*bb.v[1];
	cc5 = a.m[5]*bb.v[2];

	cc6 = a.m[6]*bb.v[0];
	cc7 = a.m[7]*bb.v[1];
	cc8 = a.m[8]*bb.v[2];
//
//	// step 2
	tc0 = c0 + c1;
	c.v[0] = tc0 + c2;
	tc3 = c3 + c4;
	c.v[1] = tc3 + c5;
	tc6 = c6 + c7;
	c.v[2] = tc6 + c8;

	tcc0 = cc0 + cc1;
	cc.v[0] = tcc0 + cc2;
	tcc3 = cc3 + cc4;
	cc.v[1] = tcc3 + cc5;
	tcc6 = cc6 + cc7;
	cc.v[2] = tcc6 + cc8;
}

#endif
