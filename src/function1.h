/* 
 * This file is part of the HPCG_FPGA distribution (https://bitbucket.org/fpgafais/hpcg_fpga).
 * Copyright (c) 2020 G. Korcyl, P. Korcyl
 * 
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU General Public License as published by  
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * 
 * File: function1.h
 * Authors: G. Korcyl, P. Korcyl
 * Contact: grzegorz.korcyl@uj.edu.pl, piotr.korcyl@uj.edu.pl
 * 
 * Version: 1.0
 * 
 * Description:
 * Wrapper for the call of the computing kernel instatiated in FPGA
 * 
 */



#ifndef __H_FUNCTION1__
#define __H_FUNCTION1__

#include <ap_int.h>
#include <iostream>
#include <fstream>
#include <string>

#include "configs.h"

extern "C" {
	void function1(ap_uint<512>* in1, ap_uint<512>* in2, ap_uint<512>* in3, ap_uint<512>* out);
	void function2(ap_uint<512>* in1, ap_uint<512>* in2, ap_uint<512>* in3, ap_uint<512>* out);
	void function3(ap_uint<512>* in1, ap_uint<512>* in2, ap_uint<512>* in3, ap_uint<512>* out);
}

#ifdef SOFTONLY

	#include "fpga_kernel.h"

	template <class T>
	void function_emu(std::vector<T, aligned_allocator<T>>& input1, std::vector<T, aligned_allocator<T>>& input2, std::vector<T, aligned_allocator<T>>& input3, std::vector<T, aligned_allocator<T>>& output, std::string id) {

		ap_uint<512>* dm1_input_ap = new ap_uint<512>[WORDS_512_SINGLE / 3];
		ap_uint<512>* dm2_input_ap = new ap_uint<512>[WORDS_512_SINGLE / 3];
		ap_uint<512>* dm3_input_ap = new ap_uint<512>[WORDS_512_SINGLE / 3];
		ap_uint<512>* dm1_output_ap = new ap_uint<512>[WORDS_512_SINGLE_OUTPUT];

//		std::ofstream outfile;
//		outfile.open("f_k" + id + "_c1.in");
		 for (int i = 0; i < WORDS_512_SINGLE / 3; i++) {
			for (int j = 0; j < 16; j++) {
		 		union {T f; unsigned long int ui; } u;
		 		u.f = input1[i * 16 + j];
		 		dm1_input_ap[i].range( (j + 1) * 32 - 1, j * 32) = u.ui;

		 		u.f = input2[i * 16 + j];
				dm2_input_ap[i].range( (j + 1) * 32 - 1, j * 32) = u.ui;

				u.f = input3[i * 16 + j];
				dm3_input_ap[i].range( (j + 1) * 32 - 1, j * 32) = u.ui;
		 	}

//		 	outfile<<dm1_input_ap[i].to_string(16, 0)<<std::endl;
		}
//		outfile.close();

////		  outfile.open("f_k" + id + "_c2.in");
//		 for (int i = 0; i < WORDS_512_SINGLE / 3; i++) {
//		  	for (int j = 0; j < 16; j++) {
//		  		union {T f; unsigned long int ui; } u;
//		  		u.f = input2[i * 16 + j];
//		  		dm2_input_ap[i].range( (j + 1) * 32 - 1, j * 32) = u.ui;
//		  	}
//
//		  	outfile<<dm2_input_ap[i].to_string(16, 0)<<std::endl;
//		 }
//		  outfile.close();
//
//		  outfile.open("f_k" + id + "_c3.in");
//		 for (int i = 0; i < WORDS_512_SINGLE / 3; i++) {
//		  	for (int j = 0; j < 16; j++) {
//		  		union {T f; unsigned long int ui; } u;
//		  		u.f = input3[i * 16 + j];
//		  		dm3_input_ap[i].range( (j + 1) * 32 - 1, j * 32) = u.ui;
//		  	}
//
//		  	outfile<<dm3_input_ap[i].to_string(16, 0)<<std::endl;
//		 }
//		 outfile.close();

//		std::cout<<"running function"<<std::endl;
//		fprintf(stderr,"running function\n");
//		printf("running function\n");

		function1(dm1_input_ap, dm2_input_ap, dm3_input_ap, dm1_output_ap);

		// float sum = 0;

//		outfile.open("f_k" + id + "_c1.out");
		for (int i = 0; i < WORDS_512_SINGLE_OUTPUT; i++) {
		 	for (int j = 0; j < 16; j++) {
		 		union {T f; unsigned long int ui; } u;
		 		u.ui = dm1_output_ap[i].range( (32 * (j + 1) - 1), (32 * j));
		 		output[i * 16 + j] = u.f;
//				if(fabs(u.f)>10e-6){
//					outfile<<(i-256)/2<<" "<< std::fixed << std::setw( 11 ) << std::setprecision( 12 )<<u.f<<std::endl;
//				}

//				sum += u.f*u.f;
			}

//		 	outfile<<dm1_output_ap[i].to_string(16, 0)<<std::endl;
		}
//		outfile.close();

//		fprintf(stderr, "TEMPORARY SUM = %1.12e\n", sum);

		delete[] dm1_input_ap;
		delete[] dm2_input_ap;
		delete[] dm3_input_ap;
		delete[] dm1_output_ap;
	}

#endif //softonly

#endif
