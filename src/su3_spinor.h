/* 
 * This file is part of the HPCG_FPGA distribution (https://bitbucket.org/fpgafais/hpcg_fpga).
 * Copyright (c) 2020 G. Korcyl, P. Korcyl
 * 
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU General Public License as published by  
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * 
 * File: su3_spinor.h
 * Authors: G. Korcyl, P. Korcyl
 * Contact: grzegorz.korcyl@uj.edu.pl, piotr.korcyl@uj.edu.pl
 * 
 * Version: 1.0
 * 
 * Description:
 * class definition and linear algebra for four-component vector class vectors
 * 
 */


#ifndef H_SU3_SPINOR
#define H_SU3_SPINOR

#include "stdlib.h"
#include "su3_vector.h"
#include <math.h>

#ifndef __SYNTHESIS__
	#include <iostream>
#endif

template <class T>
class su3_spinor {
	public:
		su3_vector<T> s[4];

		void setZero() {
			s[0].setZero();
			s[1].setZero();
			s[2].setZero();
			s[3].setZero();
		}

		T toOneNumber() {
			T sum = 0;
			for (int i = 0; i < 4; i++) {
				for (int ii = 0; ii < 6; ii+=2) {
					sum += s[i].v[ii / 2].i;
					sum += s[i].v[ii / 2].r;
				}
			}
			return sum;
		}

		su3_spinor<T> operator+(const su3_spinor &b) {
			// #pragma HLS inline
			su3_spinor<T> c;

			c.s[0] = s[0] ^ b.s[0];
			c.s[1] = s[1] ^ b.s[1];
			c.s[2] = s[2] ^ b.s[2];
			c.s[3] = s[3] ^ b.s[3];

			return c;
		}

		su3_spinor<T> operator-(const su3_spinor &b) {
			// #pragma HLS inline
			su3_spinor<T> c;

			c.s[0] = s[0] - b.s[0];
			c.s[1] = s[1] - b.s[1];
			c.s[2] = s[2] - b.s[2];
			c.s[3] = s[3] - b.s[3];


			return c;
		}

		su3_spinor<T> operator*(const T b) {
			// #pragma HLS inline
			su3_spinor<T> c;

			c.s[0] = s[0]*b;
			c.s[1] = s[1]*b;
			c.s[2] = s[2]*b;
			c.s[3] = s[3]*b;

			return c;
		}

		std::ostream& operator<<(std::ostream& stream) {
			for (int i = 0; i < 4; i++) {
				for (int ii = 0; ii < 6; ii+=2) {
					if ( fabs(s[i].v[ii / 2].r) > 10e-8)
						stream<<"s["<<i<<"].v["<<(ii/2)<<"] -> r:"<<s[i].v[ii / 2].r<<", i:"<<s[i].v[ii / 2].i<<std::endl;
				}
			}

			return stream;
		}

		void print() {
			for (int i = 0; i < 4; i++) {
				for (int ii = 0; ii < 3; ii++) {
					if ( s[i].v[ii].r*s[i].v[ii].r + s[i].v[ii].i*s[i].v[ii].i > 10e-8) {
						printf("s(%i).s[%d].v[%d] = %e, %e\n", 0, i, ii, s[i].v[ii].r, s[i].v[ii].i);
					}
				}
			}
		}

		void print(char *c) {
			#ifndef __SYNTHESIS__
			for (int i = 0; i < 4; i++) {
				for (int ii = 0; ii < 3; ii++) {
					if ( s[i].v[ii].r*s[i].v[ii].r + s[i].v[ii].i*s[i].v[ii].i > 10e-8)
						printf("s(%s).s[%d].v[%d] = %1.4e %1.4e\n", c, i, ii, s[i].v[ii].r, s[i].v[ii].i);

				}
			}
			#endif
		}


		// flatten spinor to bit vector (4 * vector.toVector (384))
		void toVector(T* r) {
			for (int i = 0; i < 4; i++) {
				#pragma HLS unroll
				for (int ii = 0; ii < 6; ii+=2) {
					#pragma HLS unroll
					r[i * 6 + ii] = s[i].v[ii / 2].i;
					r[i * 6 + ii + 1] = s[i].v[ii / 2].r;
				}
			}
		}

		T times_spinor_to_number(const su3_spinor &b) {
			T x,y,z,t;

			x = s[0].v[0].r*b.s[0].v[0].r + s[0].v[0].i*b.s[0].v[0].i
			  + s[0].v[1].r*b.s[0].v[1].r + s[0].v[1].i*b.s[0].v[1].i
			  + s[0].v[2].r*b.s[0].v[2].r + s[0].v[2].i*b.s[0].v[2].i;

			y = s[1].v[0].r*b.s[1].v[0].r + s[1].v[0].i*b.s[1].v[0].i
			  + s[1].v[1].r*b.s[1].v[1].r + s[1].v[1].i*b.s[1].v[1].i
			  + s[1].v[2].r*b.s[1].v[2].r + s[1].v[2].i*b.s[1].v[2].i;

			z = s[2].v[0].r*b.s[2].v[0].r + s[2].v[0].i*b.s[2].v[0].i
			  + s[2].v[1].r*b.s[2].v[1].r + s[2].v[1].i*b.s[2].v[1].i
			  + s[2].v[2].r*b.s[2].v[2].r + s[2].v[2].i*b.s[2].v[2].i;

			t = s[3].v[0].r*b.s[3].v[0].r + s[3].v[0].i*b.s[3].v[0].i
			  + s[3].v[1].r*b.s[3].v[1].r + s[3].v[1].i*b.s[3].v[1].i
			  + s[3].v[2].r*b.s[3].v[2].r + s[3].v[2].i*b.s[3].v[2].i;


			return x+y+z+t;
		}

};


#endif
