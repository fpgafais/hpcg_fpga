/* 
 * This file is part of the HPCG_FPGA distribution (https://bitbucket.org/fpgafais/hpcg_fpga).
 * Copyright (c) 2020 G. Korcyl, P. Korcyl
 * 
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU General Public License as published by  
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * 
 * File: host_utils.h
 * Authors: G. Korcyl, P. Korcyl
 * Contact: grzegorz.korcyl@uj.edu.pl, piotr.korcyl@uj.edu.pl
 * 
 * Version: 1.0
 * 
 * Description:
 * Helper functions for the data transformation on the host: reading input file, conversion from binary to double and float types, etc.
 * 
 */


#ifndef H_HOST_UTILS_
#define H_HOST_UTILS_

#include <iostream>

#include "configs.h"

#include "su3_spinor.h"
#include "su3_matrix.h"

#include "neighbours/su3_neighbour_array_x_forward.h"
#include "neighbours/su3_neighbour_array_x_backward.h"
#include "neighbours/su3_neighbour_array_gauge_spatial_backward.h"
#include "neighbours/su3_neighbour_array_gauge_spatial_forward.h"
#include "neighbours/su3_neighbour_array_gauge_temporal_forward.h"
#include "neighbours/su3_neighbour_array_gauge_temporal_backward.h"

template <class T>
void reconstructSpinor(T* d, su3_spinor<T>* s) {
	s->s[0].v[0].i = d[0];
	s->s[0].v[0].r = d[1];
	s->s[0].v[1].i = d[2];
	s->s[0].v[1].r = d[3];
	s->s[0].v[2].i = d[4];
	s->s[0].v[2].r = d[5];

	s->s[1].v[0].i = d[6];
	s->s[1].v[0].r = d[7];
	s->s[1].v[1].i = d[8];
	s->s[1].v[1].r = d[9];
	s->s[1].v[2].i = d[10];
	s->s[1].v[2].r = d[11];

	s->s[2].v[0].i = d[12];
	s->s[2].v[0].r = d[13];
	s->s[2].v[1].i = d[14];
	s->s[2].v[1].r = d[15];
	s->s[2].v[2].i = d[16];
	s->s[2].v[2].r = d[17];

	s->s[3].v[0].i = d[18];
	s->s[3].v[0].r = d[19];
	s->s[3].v[1].i = d[20];
	s->s[3].v[1].r = d[21];
	s->s[3].v[2].i = d[22];
	s->s[3].v[2].r = d[23];
}

template <class T>
int set_initial_U(su3_matrix<T>** U, char c, char* input_file){

	int i,j,k,l;
	int tmp1,tmp2,tmp3,tmp4,tmp5,tmp6,tmp7;

	FILE *f;

	switch(c){

	case 'r':

//		f = fopen("output_small.txt", "r+");
		// f = fopen("output_1024.txt", "r+");
		f = fopen(input_file, "r+");
		printf("%s\n", input_file);
		if (f == 0) {
			printf("ERROR: reading input data file, aborting\n");
			exit(1);
		}
		
		double in_b, in_c;

        int jj;

        for(jj = 0; jj < 4; jj++){

			if(jj == 3)
					j = 0;
			if(jj == 0)
					j = 1;
			if(jj == 1)
					j = 2;
			if(jj == 2)
					j = 3;

			for(i = 0; i < Vv; i++){

	      			for(k = 0; k < 3; k++){
					for(l = 0; l < 3; l++){
			
						fscanf(f,"%i   %i %i %i %i %i %i   %lf   %lf\n", &tmp1, &tmp2, &tmp3, &tmp4, &tmp5, &tmp6, &tmp7, &in_b, &in_c);
						U[i][j].m[3*k+l].r = static_cast<T>(in_b);
						U[i][j].m[3*k+l].i = static_cast<T>(in_c);

					}
				}

			}
		}

		fclose(f);

		break;
	}

    return 1;
}


template <class T>
int set_initial_x(su3_spinor<T>* x, char c){

	int i;

	switch(c){

	case 'u':

		for(i = 0; i < Vv; i++){
			x[i].setOne();
		}

		break;

	case 'r':

		for(i = 0; i < Vv; i++){
			x[i].setRandom();
		}

		break;

	}

    return 1;
}

template <class T>
int set_initial_b(su3_spinor<T>* b){

	int i;

	for(i = 0; i < Vv; i++){
		b[i].setZero();
	}

	b[0].s[0].v[0] = 1;

    return 1;
}

template <class T>
void flattenSpinor(su3_spinor<T>* s, T* r) {
    s->toVector(r);
}

template <class T>
void flattenMatrix(su3_matrix<T>* m, T* r, int size) {
    T dd[18];

	for (int i = 0; i < size; i++) {
		m[i].toVector(dd);
		for (int jj = 0; jj < 18; jj++) {
			r[18 * i + jj] = dd[jj];
		}
	}
}

 template <class T>
 void prepareForTransmissionBuffers(su3_matrix<T>* u_f, su3_spinor<T> x, int index, T* buf0, T* buf1, T* buf2) {
         // DDR area allocation (512b = 16 x float)
         // I             DDR0                                   DDR1                                    DDR2
         // 0 (15-0)      u_f[0](15-0)                           u_f[2](11-0) | u_f[1](17-14)            x(7-0) | u_f[3](17-10)
         // 1 (31-16)     u_f[1](13-0) | u_f[0](17-16)           u_f[3](9-0) | u_f[2](17-12)             x(23-8)

         //int offset = 32;
	 	 int offset = 32;

         T matrix[4 * 18];

         flattenMatrix<T>(u_f, matrix, 4);

         for (int i = 0; i < 32; i++) {
                 buf0[(index * offset) + i] = matrix[i];
         }

         for (int i = 0; i < 32; i++) {
                 buf1[(index * offset) + i] = matrix[i + 32];
         }

         for (int i = 0; i < 8; i++) {
                 buf2[(index * offset) + i] = matrix[i + 32 + 32];
         }

         flattenSpinor<T>(&x, &buf2[(index * offset) + 8]);
 }

template <class T>
void prepareForTransmissionBuffers(su3_matrix<T>* u_f, su3_spinor<T> x, int index, T* buf0) {
        // DDR area allocation (512b = 16 x float)

        // I             DDR0  
        // 0 (15-0)      u_f[0](15-0)                                       
        // 1 (31-16)     u_f[1](13-0) | u_f[0](17-16)                        
        // 2 (47-32)     u_f[2](11-0) | u_f[1](17-14)
        // 3 (63-48)     u_f[3](9-0)  | u_f[2](17-12)
        // 4 (79-64)     x(7-0)       | u_f[3](17-10)
        // 5 (95-80)     x(23-8)


        int offset = 96;

        T matrix[4 * 18];

        flattenMatrix<T>(u_f, &buf0[index * offset], 4);
        flattenSpinor<T>(&x, &buf0[(index * offset) + 4 * 18]);
}

template <class T>
void multBatch(
		su3_matrix<T> *U_l_temporal, su3_matrix<T> *U_u_temporal,
		su3_matrix<T> *U_l_spatial, su3_matrix<T> *U_u_spatial,
		su3_spinor<T> *x_l, su3_spinor<T> *x_u,
		char c,
                // pointers to data buffers for transmission
                T* buf0, T* buf1, T* buf2
                ) {

        int tt,zz,yy,xx;

        if(c == 'd'){
                for(tt = 0; tt <= Tt/2+1; tt++){
                        for(zz = 0; zz < Nn; zz++){
                                for(yy = 0; yy < Nn; yy++){
                                        for(xx = 0; xx < Nn; xx++){

                                                int i = tt*Nn*Nn*Nn + zz*Nn*Nn + yy*Nn + xx;

                                                int ii, iii;

                                                for(ii = 2; ii < 4; ii++){
                                                        for(iii = 0; iii < 3; iii++){

                                                                
								x_l[i].s[ii].v[iii].i *= -1;
                                                                x_l[i].s[ii].v[iii].r *= -1;

                                                                x_u[i].s[ii].v[iii].i *= -1;
                                                                x_u[i].s[ii].v[iii].r *= -1;
                                                        }
                                                }
                                        }
                                }
                        }
                }
        }

        // do the job
        for(tt = 0; tt <= Tt/2+1; tt++){
		for(zz = 0; zz < Nn; zz++){
                        for(yy = 0; yy < Nn; yy++){
                                for(xx = 0; xx < Nn; xx++){

                                        int i = tt*Nn*Nn*Nn + zz*Nn*Nn + yy*Nn + xx;

                                        int ind_a[8];

                                        ind_a[7] = i;
                                        ind_a[6] = (i - Nn*Nn*Nn) + 2*Vspatial_half_gauge;
                                        ind_a[5] = (i - Nn*Nn*Nn) +   Vspatial_half_gauge;
                                        ind_a[4] = (i - Nn*Nn*Nn);

                                        su3_matrix<T> u_f_full[4];

					if(tt == 0){

						for(int is = 0; is < 3; is++) {
							u_f_full[is].setZero();
                        	                }
						u_f_full[3] = U_l_temporal[i];
       

					}else if(tt > 0 && tt < Tt/2+1){

						for(int is = 0; is < 3; is++) {
							u_f_full[is] = U_l_spatial[ind_a[is+4]];
						}
						u_f_full[3] = U_l_temporal[ind_a[7]];



					}else if(tt == Tt/2+1){
                                	        for(int is = 0; is < 4; is++) {
                                        	        u_f_full[is].setZero();
	                                        }
					}

					prepareForTransmissionBuffers(&u_f_full[0], x_l[i], i, buf0, buf1, buf2);
                                }
                        }
                }
        }

        for(tt = 0; tt <= Tt/2+1; tt++){
                for(zz = 0; zz < Nn; zz++){
                        for(yy = 0; yy < Nn; yy++){
                                for(xx = 0; xx < Nn; xx++){

                                        int i = tt*Nn*Nn*Nn + zz*Nn*Nn + yy*Nn + xx;

                                        int ind_a[8];

                                        ind_a[7] = i;
                                        ind_a[6] = (i - Nn*Nn*Nn) + 2*Vspatial_half_gauge;
                                        ind_a[5] = (i - Nn*Nn*Nn) +   Vspatial_half_gauge;
                                        ind_a[4] = (i - Nn*Nn*Nn);

                                        su3_matrix<T> u_f_full[4];

 	                                if(tt == 0){
 
                                                for(int is = 0; is < 3; is++) {
                                                        u_f_full[is].setZero();
                                                }
                                                u_f_full[3] = U_u_temporal[i];


                                        }else if(tt > 0 && tt < Tt/2+1){

						for(int is = 0; is < 3; is++) {
							u_f_full[is] = U_u_spatial[ind_a[is+4]];
						}
						u_f_full[3] = U_u_temporal[ind_a[7]];

                                        }else if(tt == Tt/2+1){
                                                for(int is = 0; is < 4; is++) {
                                                        u_f_full[is].setZero();
                                                }
                                        }

					prepareForTransmissionBuffers(&u_f_full[0], x_u[i], i + (Tt/2+2)*Nn*Nn*Nn, buf0, buf1, buf2);
                                }
                        }
                }
        }
}



#endif
