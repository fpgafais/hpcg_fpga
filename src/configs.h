/* 
 * This file is part of the HPCG_FPGA distribution (https://bitbucket.org/fpgafais/hpcg_fpga).
 * Copyright (c) 2020 G. Korcyl, P. Korcyl
 * 
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU General Public License as published by  
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * 
 * File: config.h
 * Authors: G. Korcyl, P. Korcyl
 * Contact: grzegorz.korcyl@uj.edu.pl, piotr.korcyl@uj.edu.pl
 * 
 * Version: 1.0
 * 
 * Description:
 * Collection of all constants which have to be fixed at compile time
 * 
 */


#ifndef __H_CONFIGS__
#define __H_CONFIGS__

#define Nn 4
#define Tt 4
#define Vv (Nn*Nn*Nn*Tt)


#define kappa 0.12195


#define Vspatial (Nn*Nn*Nn)

#define Vspatial_half_gauge ((Tt/2)*Nn*Nn*Nn)
#define Vtemporal_half_gauge ((Tt/2+1)*Nn*Nn*Nn)

#define Vhalf_x ((Tt/2+2)*Nn*Nn*Nn)

#define NUMBER_OF_DATASETS Vv
#define VALUES_IN_DATASET 96
#define WORDS_IN_DATASETS (NUMBER_OF_DATASETS)*VALUES_IN_DATASET
#define WORDS_IN_DATASETS2 (NUMBER_OF_DATASETS+4*Nn*Nn*Nn)*VALUES_IN_DATASET
#define WORDS_512_SINGLE WORDS_IN_DATASETS2/16
#define WORDS_512_DOUBLE WORDS_IN_DATASETS2/8
#define WORDS_512_SINGLE_OUTPUT (NUMBER_OF_DATASETS+4*Nn*Nn*Nn)*2
#define OUTPUT_SIZE (NUMBER_OF_DATASETS+4*Nn*Nn*Nn)*32

#define CYCLES_PER_ITER 2

typedef float low_type;
typedef float high_type;


#endif
