#!/usr/bin/perl

##/* 
## * This file is part of the XXX distribution (https://github.com/xxxx or http://xxx.github.io).
## * Copyright (c) 2020 G. Korcyl, P. Korcyl
## * 
## * This program is free software: you can redistribute it and/or modify  
## * it under the terms of the GNU General Public License as published by  
## * the Free Software Foundation, version 3.
## *
## * This program is distributed in the hope that it will be useful, but 
## * WITHOUT ANY WARRANTY; without even the implied warranty of 
## * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
## * General Public License for more details.
## *
## * You should have received a copy of the GNU General Public License 
## * along with this program. If not, see <http://www.gnu.org/licenses/>.
## * 
## * File: generate_neighbour_array.pl
## * Authors: G. Korcyl, P. Korcyl
## * Contact: grzegorz.korcyl@uj.edu.pl, piotr.korcyl@uj.edu.pl
## * 
## * Version: 1.0
## * 
## * Description:
## * Script to generate .data files containing tables with locations of neighbouring lattice sites. Lattice 
## * sizes N and T have to be adapted by hand.
## * 
## */

use Switch;

my $N = 4;
my $T = 4;
#my $N = 4;
#my $T = 4;
my $V = $T*$N*$N*$N;


my $ctr = 0;

my $file = "neighbour_array_x_forward.data";
open(my $fh, '>', $file);

##(T/2+2)*N*N*N

for(my $t = 0; $t < $T/2+2; $t++){
for(my $z = 0; $z < $N; $z++){
for(my $y = 0; $y < $N; $y++){
for(my $x = 0; $x < $N; $x++){
##    for(my $dir = 0; $dir < 2; $dir++){
	my $dir = 1;
        for(my $axis = 0; $axis < 4; $axis++){
#            my $t = ($pos/($N*$N*$N));
#            my $z = ((($pos)%($N*$N*$N))/($N*$N));
#            my $y = ((($pos)%($N*$N*$N))%($N*$N)/$N);
#            my $x = (((($pos)%($N*$N*$N))%($N*$N))%$N);

            my $x_new, $y_new, $z_new, $t_new;

            #first simply take the neighbour by adding +/-1 (FWD=1,BWD=0 => +1=2*FWD-1, -1=2*BWD-1)

            switch($axis){
                case 0 {
                    $x_new = $x + 2*$dir-1;
                    $y_new = $y;
                    $z_new = $z;
                    $t_new = $t;
                }
                case 1 {
                    $x_new = $x;
                    $y_new = $y + 2*$dir-1;
                    $z_new = $z;
                    $t_new = $t;
                }
                case 2 {
                    $x_new = $x;
                    $y_new = $y;
                    $z_new = $z + 2*$dir-1;
                    $t_new = $t;
                }
                case 3 {
                    $x_new = $x;
                    $y_new = $y;
                    $z_new = $z;
                    $t_new = $t + 2*$dir-1;
                }
            }   
            
            #periodic boundary conditions; won't use that anyway
            if($x_new < 0){
                $x_new = $N-1;
            }elsif($x_new > $N-1){
                $x_new = 0;
            }

            if($y_new < 0){
                $y_new = $N-1;
            }elsif($y_new > $N-1){
                $y_new = 0;
            }

            if($z_new < 0){
                $z_new = $N-1;
            }elsif($z_new > $N-1){
                $z_new = 0;
            }

            if($t_new < 0){
                $t_new = $T/2+2-1;
            }elsif($t_new > $T/2+2-1){
                $t_new = 0;
            }

            if ($ctr < $V * 2 * 4 - 1) {
                print $fh (int ($t_new*$N*$N*$N + $z_new*$N*$N + $y_new*$N + $x_new)).", ";
            }
            else {
                print $fh (int ($t_new*$N*$N*$N + $z_new*$N*$N + $y_new*$N + $x_new));
            }
            $ctr++;

        }#$axis
    ##}#$dir
}}}}#$pos

close $fh;


my $ctr = 0;

my $file = "neighbour_array_x_backward.data";
open(my $fh, '>', $file);

##(T/2+2)*N*N*N

for(my $t = 0; $t < $T/2+2; $t++){
for(my $z = 0; $z < $N; $z++){
for(my $y = 0; $y < $N; $y++){
for(my $x = 0; $x < $N; $x++){
##    for(my $dir = 0; $dir < 2; $dir++){
	my $dir = 0;
        for(my $axis = 0; $axis < 4; $axis++){
#            my $t = ($pos/($N*$N*$N));
#            my $z = ((($pos)%($N*$N*$N))/($N*$N));
#            my $y = ((($pos)%($N*$N*$N))%($N*$N)/$N);
#            my $x = (((($pos)%($N*$N*$N))%($N*$N))%$N);

            my $x_new, $y_new, $z_new, $t_new;

            #first simply take the neighbour by adding +/-1 (FWD=1,BWD=0 => +1=2*FWD-1, -1=2*BWD-1)

            switch($axis){
                case 0 {
                    $x_new = $x + 2*$dir-1;
                    $y_new = $y;
                    $z_new = $z;
                    $t_new = $t;
                }
                case 1 {
                    $x_new = $x;
                    $y_new = $y + 2*$dir-1;
                    $z_new = $z;
                    $t_new = $t;
                }
                case 2 {
                    $x_new = $x;
                    $y_new = $y;
                    $z_new = $z + 2*$dir-1;
                    $t_new = $t;
                }
                case 3 {
                    $x_new = $x;
                    $y_new = $y;
                    $z_new = $z;
                    $t_new = $t + 2*$dir-1;
                }
            }   
            
            #periodic boundary conditions; won't use that anyway
            if($x_new < 0){
                $x_new = $N-1;
            }elsif($x_new > $N-1){
                $x_new = 0;
            }

            if($y_new < 0){
                $y_new = $N-1;
            }elsif($y_new > $N-1){
                $y_new = 0;
            }

            if($z_new < 0){
                $z_new = $N-1;
            }elsif($z_new > $N-1){
                $z_new = 0;
            }

            if($t_new < 0){
                $t_new = $T/2+2-1;
            }elsif($t_new > $T/2+2-1){
                $t_new = 0;
            }

            if ($ctr < $V * 2 * 4 - 1) {
                print $fh (int ($t_new*$N*$N*$N + $z_new*$N*$N + $y_new*$N + $x_new)).", ";
            }
            else {
                print $fh (int ($t_new*$N*$N*$N + $z_new*$N*$N + $y_new*$N + $x_new));
            }
            $ctr++;

        }#$axis
    ##}#$dir
}}}}#$pos

close $fh;





###define Vspatial_half_gauge ((T/2)*N*N*N) 
###define Vtemporal_half_gauge ((T/2+1)*N*N*N) 


my $ctr = 0;

my $file = "neighbour_array_gauge_temporal_forward.data";
open(my $fh, '>', $file);

for(my $t = 0; $t < $T/2+1; $t++){
for(my $z = 0; $z < $N; $z++){
for(my $y = 0; $y < $N; $y++){
for(my $x = 0; $x < $N; $x++){
##    for(my $dir = 0; $dir < 2; $dir++){
	my $dir = 1;
        for(my $axis = 0; $axis < 4; $axis++){
#            my $t = ($pos/($N*$N*$N));
#            my $z = ((($pos)%($N*$N*$N))/($N*$N));
#            my $y = ((($pos)%($N*$N*$N))%($N*$N)/$N);
#            my $x = (((($pos)%($N*$N*$N))%($N*$N))%$N);

            my $x_new, $y_new, $z_new, $t_new;

            #first simply take the neighbour by adding +/-1 (FWD=1,BWD=0 => +1=2*FWD-1, -1=2*BWD-1)

            switch($axis){
                case 0 {
                    $x_new = $x + 2*$dir-1;
                    $y_new = $y;
                    $z_new = $z;
                    $t_new = $t;
                }
                case 1 {
                    $x_new = $x;
                    $y_new = $y + 2*$dir-1;
                    $z_new = $z;
                    $t_new = $t;
                }
                case 2 {
                    $x_new = $x;
                    $y_new = $y;
                    $z_new = $z + 2*$dir-1;
                    $t_new = $t;
                }
                case 3 {
                    $x_new = $x;
                    $y_new = $y;
                    $z_new = $z;
                    $t_new = $t + 2*$dir-1;
                }
            }   
            
            #periodic boundary conditions; won't use that anyway
            if($x_new < 0){
                $x_new = $N-1;
            }elsif($x_new > $N-1){
                $x_new = 0;
            }

            if($y_new < 0){
                $y_new = $N-1;
            }elsif($y_new > $N-1){
                $y_new = 0;
            }

            if($z_new < 0){
                $z_new = $N-1;
            }elsif($z_new > $N-1){
                $z_new = 0;
            }

            if($t_new < 0){
                $t_new = $T/2+1-1;
            }elsif($t_new > $T/2+1-1){
                $t_new = 0;
            }

            if ($ctr < $V * 2 * 4 - 1) {
                print $fh (int ($t_new*$N*$N*$N + $z_new*$N*$N + $y_new*$N + $x_new)).", ";
            }
            else {
                print $fh (int ($t_new*$N*$N*$N + $z_new*$N*$N + $y_new*$N + $x_new));
            }
            $ctr++;

        }#$axis
##    }#$dir
}}}}#$pos

close $fh;

my $ctr = 0;

my $file = "neighbour_array_gauge_temporal_backward.data";
open(my $fh, '>', $file);

for(my $t = 0; $t < $T/2+1; $t++){
for(my $z = 0; $z < $N; $z++){
for(my $y = 0; $y < $N; $y++){
for(my $x = 0; $x < $N; $x++){
##    for(my $dir = 0; $dir < 2; $dir++){
	my $dir = 0;
        for(my $axis = 0; $axis < 4; $axis++){
#            my $t = ($pos/($N*$N*$N));
#            my $z = ((($pos)%($N*$N*$N))/($N*$N));
#            my $y = ((($pos)%($N*$N*$N))%($N*$N)/$N);
#            my $x = (((($pos)%($N*$N*$N))%($N*$N))%$N);

            my $x_new, $y_new, $z_new, $t_new;

            #first simply take the neighbour by adding +/-1 (FWD=1,BWD=0 => +1=2*FWD-1, -1=2*BWD-1)

            switch($axis){
                case 0 {
                    $x_new = $x + 2*$dir-1;
                    $y_new = $y;
                    $z_new = $z;
                    $t_new = $t;
                }
                case 1 {
                    $x_new = $x;
                    $y_new = $y + 2*$dir-1;
                    $z_new = $z;
                    $t_new = $t;
                }
                case 2 {
                    $x_new = $x;
                    $y_new = $y;
                    $z_new = $z + 2*$dir-1;
                    $t_new = $t;
                }
                case 3 {
                    $x_new = $x;
                    $y_new = $y;
                    $z_new = $z;
                    $t_new = $t + 2*$dir-1;
                }
            }   
            
            #periodic boundary conditions; won't use that anyway
            if($x_new < 0){
                $x_new = $N-1;
            }elsif($x_new > $N-1){
                $x_new = 0;
            }

            if($y_new < 0){
                $y_new = $N-1;
            }elsif($y_new > $N-1){
                $y_new = 0;
            }

            if($z_new < 0){
                $z_new = $N-1;
            }elsif($z_new > $N-1){
                $z_new = 0;
            }

            if($t_new < 0){
                $t_new = $T/2+1-1;
            }elsif($t_new > $T/2+1-1){
                $t_new = 0;
            }

            if ($ctr < $V * 2 * 4 - 1) {
                print $fh (int ($t_new*$N*$N*$N + $z_new*$N*$N + $y_new*$N + $x_new)).", ";
            }
            else {
                print $fh (int ($t_new*$N*$N*$N + $z_new*$N*$N + $y_new*$N + $x_new));
            }
            $ctr++;

        }#$axis
##    }#$dir
}}}}#$pos

close $fh;

###define Vspatial_half_gauge ((T/2)*N*N*N) 
###define Vtemporal_half_gauge ((T/2+1)*N*N*N) 

my $ctr = 0;

my $file = "neighbour_array_gauge_spatial_forward.data";
open(my $fh, '>', $file);

for(my $t = 0; $t < $T/2; $t++){
for(my $z = 0; $z < $N; $z++){
for(my $y = 0; $y < $N; $y++){
for(my $x = 0; $x < $N; $x++){
##    for(my $dir = 0; $dir < 2; $dir++){
	my $dir = 1;
        for(my $axis = 0; $axis < 4; $axis++){
#            my $t = ($pos/($N*$N*$N));
#            my $z = ((($pos)%($N*$N*$N))/($N*$N));
#            my $y = ((($pos)%($N*$N*$N))%($N*$N)/$N);
#            my $x = (((($pos)%($N*$N*$N))%($N*$N))%$N);

            my $x_new, $y_new, $z_new, $t_new;

            #first simply take the neighbour by adding +/-1 (FWD=1,BWD=0 => +1=2*FWD-1, -1=2*BWD-1)

            switch($axis){
                case 0 {
                    $x_new = $x + 2*$dir-1;
                    $y_new = $y;
                    $z_new = $z;
                    $t_new = $t;
                }
                case 1 {
                    $x_new = $x;
                    $y_new = $y + 2*$dir-1;
                    $z_new = $z;
                    $t_new = $t;
                }
                case 2 {
                    $x_new = $x;
                    $y_new = $y;
                    $z_new = $z + 2*$dir-1;
                    $t_new = $t;
                }
                case 3 {
                    $x_new = $x;
                    $y_new = $y;
                    $z_new = $z;
                    $t_new = $t + 2*$dir-1;
                }
            }   
            
            #periodic boundary conditions; won't use that anyway
            if($x_new < 0){
                $x_new = $N-1;
            }elsif($x_new > $N-1){
                $x_new = 0;
            }

            if($y_new < 0){
                $y_new = $N-1;
            }elsif($y_new > $N-1){
                $y_new = 0;
            }

            if($z_new < 0){
                $z_new = $N-1;
            }elsif($z_new > $N-1){
                $z_new = 0;
            }

            if($t_new < 0){
                $t_new = $T/2-1;
            }elsif($t_new > $T/2-1){
                $t_new = 0;
            }

            if ($ctr < $V * 2 * 4 - 1) {
                print $fh (int ($t_new*$N*$N*$N + $z_new*$N*$N + $y_new*$N + $x_new)).", ";
            }
            else {
                print $fh (int ($t_new*$N*$N*$N + $z_new*$N*$N + $y_new*$N + $x_new));
            }
            $ctr++;

        }#$axis
##    }#$dir
}}}}#$pos

close $fh;

my $ctr = 0;

my $file = "neighbour_array_gauge_spatial_backward.data";
open(my $fh, '>', $file);

for(my $t = 0; $t < $T/2; $t++){
for(my $z = 0; $z < $N; $z++){
for(my $y = 0; $y < $N; $y++){
for(my $x = 0; $x < $N; $x++){
##    for(my $dir = 0; $dir < 2; $dir++){
	my $dir = 0;
        for(my $axis = 0; $axis < 4; $axis++){
#            my $t = ($pos/($N*$N*$N));
#            my $z = ((($pos)%($N*$N*$N))/($N*$N));
#            my $y = ((($pos)%($N*$N*$N))%($N*$N)/$N);
#            my $x = (((($pos)%($N*$N*$N))%($N*$N))%$N);

            my $x_new, $y_new, $z_new, $t_new;

            #first simply take the neighbour by adding +/-1 (FWD=1,BWD=0 => +1=2*FWD-1, -1=2*BWD-1)

            switch($axis){
                case 0 {
                    $x_new = $x + 2*$dir-1;
                    $y_new = $y;
                    $z_new = $z;
                    $t_new = $t;
                }
                case 1 {
                    $x_new = $x;
                    $y_new = $y + 2*$dir-1;
                    $z_new = $z;
                    $t_new = $t;
                }
                case 2 {
                    $x_new = $x;
                    $y_new = $y;
                    $z_new = $z + 2*$dir-1;
                    $t_new = $t;
                }
                case 3 {
                    $x_new = $x;
                    $y_new = $y;
                    $z_new = $z;
                    $t_new = $t + 2*$dir-1;
                }
            }   
            
            #periodic boundary conditions; won't use that anyway
            if($x_new < 0){
                $x_new = $N-1;
            }elsif($x_new > $N-1){
                $x_new = 0;
            }

            if($y_new < 0){
                $y_new = $N-1;
            }elsif($y_new > $N-1){
                $y_new = 0;
            }

            if($z_new < 0){
                $z_new = $N-1;
            }elsif($z_new > $N-1){
                $z_new = 0;
            }

            if($t_new < 0){
                $t_new = $T/2-1;
            }elsif($t_new > $T/2-1){
                $t_new = 0;
            }

            if ($ctr < $V * 2 * 4 - 1) {
                print $fh (int ($t_new*$N*$N*$N + $z_new*$N*$N + $y_new*$N + $x_new)).", ";
            }
            else {
                print $fh (int ($t_new*$N*$N*$N + $z_new*$N*$N + $y_new*$N + $x_new));
            }
            $ctr++;

        }#$axis
##    }#$dir
}}}}#$pos

close $fh;


my $ctr = 0;

my $file = "neighbour_bufer_x_forward.data";
open(my $fh, '>', $file);

##(T/2+2)*N*N*N

for(my $z = 0; $z < $N; $z++){
for(my $y = 0; $y < $N; $y++){
for(my $x = 0; $x < $N; $x++){
##    for(my $dir = 0; $dir < 2; $dir++){
	my $dir = 1;
#            my $t = ($pos/($N*$N*$N));
#            my $z = ((($pos)%($N*$N*$N))/($N*$N));
#            my $y = ((($pos)%($N*$N*$N))%($N*$N)/$N);
#            my $x = (((($pos)%($N*$N*$N))%($N*$N))%$N);

            my $x_new, $y_new, $z_new, $t_new;

            #first simply take the neighbour by adding +/-1 (FWD=1,BWD=0 => +1=2*FWD-1, -1=2*BWD-1)
            $x_new = $x + 2*$dir-1;
            $y_new = $y;
            $z_new = $z;
           
            #periodic boundary conditions; won't use that anyway
            if($x_new < 0){
                $x_new = $N-1;
            }elsif($x_new > $N-1){
                $x_new = 0;
            }

            if ($ctr < $V * 2 * 4 - 1) {
                print $fh (int ($z_new*$N*$N + $y_new*$N + $x_new)).", ";
            }
            else {
                print $fh (int ($z_new*$N*$N + $y_new*$N + $x_new));
            }
            $ctr++;
}}}#$pos

close $fh;


my $ctr = 0;

my $file = "neighbour_bufer_x_backward.data";
open(my $fh, '>', $file);

##(T/2+2)*N*N*N

for(my $z = 0; $z < $N; $z++){
for(my $y = 0; $y < $N; $y++){
for(my $x = 0; $x < $N; $x++){
##    for(my $dir = 0; $dir < 2; $dir++){
	my $dir = 0;
#            my $t = ($pos/($N*$N*$N));
#            my $z = ((($pos)%($N*$N*$N))/($N*$N));
#            my $y = ((($pos)%($N*$N*$N))%($N*$N)/$N);
#            my $x = (((($pos)%($N*$N*$N))%($N*$N))%$N);

            my $x_new, $y_new, $z_new, $t_new;

            #first simply take the neighbour by adding +/-1 (FWD=1,BWD=0 => +1=2*FWD-1, -1=2*BWD-1)
            $x_new = $x + 2*$dir-1;
            $y_new = $y;
            $z_new = $z;
           
            #periodic boundary conditions; won't use that anyway
            if($x_new < 0){
                $x_new = $N-1;
            }elsif($x_new > $N-1){
                $x_new = 0;
            }

            if ($ctr < $V * 2 * 4 - 1) {
                print $fh (int ($z_new*$N*$N + $y_new*$N + $x_new)).", ";
            }
            else {
                print $fh (int ($z_new*$N*$N + $y_new*$N + $x_new));
            }
            $ctr++;
}}}#$pos

close $fh;



my $ctr = 0;

my $file = "neighbour_bufer_y_forward.data";
open(my $fh, '>', $file);

##(T/2+2)*N*N*N

for(my $z = 0; $z < $N; $z++){
for(my $y = 0; $y < $N; $y++){
for(my $x = 0; $x < $N; $x++){
##    for(my $dir = 0; $dir < 2; $dir++){
	my $dir = 1;
#            my $t = ($pos/($N*$N*$N));
#            my $z = ((($pos)%($N*$N*$N))/($N*$N));
#            my $y = ((($pos)%($N*$N*$N))%($N*$N)/$N);
#            my $x = (((($pos)%($N*$N*$N))%($N*$N))%$N);

            my $x_new, $y_new, $z_new, $t_new;

            #first simply take the neighbour by adding +/-1 (FWD=1,BWD=0 => +1=2*FWD-1, -1=2*BWD-1)
            $y_new = $y + 2*$dir-1;
            $x_new = $x;
            $z_new = $z;
           
            #periodic boundary conditions; won't use that anyway
            if($y_new < 0){
                $y_new = $N-1;
            }elsif($y_new > $N-1){
                $y_new = 0;
            }

            if ($ctr < $V * 2 * 4 - 1) {
                print $fh (int ($z_new*$N*$N + $y_new*$N + $x_new)).", ";
            }
            else {
                print $fh (int ($z_new*$N*$N + $y_new*$N + $x_new));
            }
            $ctr++;
}}}#$pos

close $fh;


my $ctr = 0;

my $file = "neighbour_bufer_y_backward.data";
open(my $fh, '>', $file);

##(T/2+2)*N*N*N

for(my $z = 0; $z < $N; $z++){
for(my $y = 0; $y < $N; $y++){
for(my $x = 0; $x < $N; $x++){
##    for(my $dir = 0; $dir < 2; $dir++){
	my $dir = 0;
#            my $t = ($pos/($N*$N*$N));
#            my $z = ((($pos)%($N*$N*$N))/($N*$N));
#            my $y = ((($pos)%($N*$N*$N))%($N*$N)/$N);
#            my $x = (((($pos)%($N*$N*$N))%($N*$N))%$N);

            my $x_new, $y_new, $z_new, $t_new;

            #first simply take the neighbour by adding +/-1 (FWD=1,BWD=0 => +1=2*FWD-1, -1=2*BWD-1)
            $y_new = $y + 2*$dir-1;
            $x_new = $x;
            $z_new = $z;
           
            #periodic boundary conditions; won't use that anyway
            if($y_new < 0){
                $y_new = $N-1;
            }elsif($y_new > $N-1){
                $y_new = 0;
            }

            if ($ctr < $V * 2 * 4 - 1) {
                print $fh (int ($z_new*$N*$N + $y_new*$N + $x_new)).", ";
            }
            else {
                print $fh (int ($z_new*$N*$N + $y_new*$N + $x_new));
            }
            $ctr++;
}}}#$pos

close $fh;


my $ctr = 0;

my $file = "neighbour_bufer_z_forward.data";
open(my $fh, '>', $file);

##(T/2+2)*N*N*N

for(my $z = 0; $z < $N; $z++){
for(my $y = 0; $y < $N; $y++){
for(my $x = 0; $x < $N; $x++){
##    for(my $dir = 0; $dir < 2; $dir++){
	my $dir = 1;
#            my $t = ($pos/($N*$N*$N));
#            my $z = ((($pos)%($N*$N*$N))/($N*$N));
#            my $y = ((($pos)%($N*$N*$N))%($N*$N)/$N);
#            my $x = (((($pos)%($N*$N*$N))%($N*$N))%$N);

            my $x_new, $y_new, $z_new, $t_new;

            #first simply take the neighbour by adding +/-1 (FWD=1,BWD=0 => +1=2*FWD-1, -1=2*BWD-1)
            $z_new = $z + 2*$dir-1;
            $y_new = $y;
            $x_new = $x;
           
            #periodic boundary conditions; won't use that anyway
            if($z_new < 0){
                $z_new = $N-1;
            }elsif($z_new > $N-1){
                $z_new = 0;
            }

            if ($ctr < $V * 2 * 4 - 1) {
                print $fh (int ($z_new*$N*$N + $y_new*$N + $x_new)).", ";
            }
            else {
                print $fh (int ($z_new*$N*$N + $y_new*$N + $x_new));
            }
            $ctr++;
}}}#$pos

close $fh;


my $ctr = 0;

my $file = "neighbour_bufer_z_backward.data";
open(my $fh, '>', $file);

##(T/2+2)*N*N*N

for(my $z = 0; $z < $N; $z++){
for(my $y = 0; $y < $N; $y++){
for(my $x = 0; $x < $N; $x++){
##    for(my $dir = 0; $dir < 2; $dir++){
	my $dir = 0;
#            my $t = ($pos/($N*$N*$N));
#            my $z = ((($pos)%($N*$N*$N))/($N*$N));
#            my $y = ((($pos)%($N*$N*$N))%($N*$N)/$N);
#            my $x = (((($pos)%($N*$N*$N))%($N*$N))%$N);

            my $x_new, $y_new, $z_new, $t_new;

            #first simply take the neighbour by adding +/-1 (FWD=1,BWD=0 => +1=2*FWD-1, -1=2*BWD-1)
            $z_new = $z + 2*$dir-1;
            $y_new = $y;
            $x_new = $x;
           
            #periodic boundary conditions; won't use that anyway
            if($z_new < 0){
                $z_new = $N-1;
            }elsif($z_new > $N-1){
                $z_new = 0;
            }

            if ($ctr < $V * 2 * 4 - 1) {
                print $fh (int ($z_new*$N*$N + $y_new*$N + $x_new)).", ";
            }
            else {
                print $fh (int ($z_new*$N*$N + $y_new*$N + $x_new));
            }
            $ctr++;
}}}#$pos

close $fh;
