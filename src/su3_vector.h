/* 
 * This file is part of the HPCG_FPGA distribution (https://bitbucket.org/fpgafais/hpcg_fpga).
 * Copyright (c) 2020 G. Korcyl, P. Korcyl
 * 
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU General Public License as published by  
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * 
 * File: su3_vector.h
 * Authors: G. Korcyl, P. Korcyl
 * Contact: grzegorz.korcyl@uj.edu.pl, piotr.korcyl@uj.edu.pl
 * 
 * Version: 1.0
 * 
 * Description:
 * class definition and linear algebra for three-component complex class vectors
 * 
 */


#ifndef H_SU3_VECTOR
#define H_SU3_VECTOR

#include "su3_complex.h"

template <class T>
class su3_vector {
	public:
		su3_complex<T> v[3];

		void setZero() {
			v[0].r = 0.0;
			v[1].r = 0.0;
			v[2].r = 0.0;	
			v[0].i = 0.0;
			v[1].i = 0.0;
			v[2].i = 0.0;	
		}

		su3_vector<T> operator!() {
			// #pragma HLS inline
			su3_vector<T> c;
			
			for (int j = 0; j < 3; j++) {
			#pragma HLS unroll
				c.v[j].r = -v[j].i;
				c.v[j].i = v[j].r;
			}

			return c;
		}

		su3_vector<T> operator+(const su3_vector<T> &b) {
			// #pragma HLS inline
			su3_vector<T> c;

			for (int j = 0; j < 3; j++) {
				#pragma HLS unroll
				c.v[j].i = v[j].i + b.v[j].i;
				c.v[j].r = v[j].r + b.v[j].r;
			}

			return c;
		}

		su3_vector<T> operator*(const T b) {
			// #pragma HLS inline
			su3_vector<T> c;

			for (int j = 0; j < 3; j++) {
				#pragma HLS unroll
				c.v[j] = v[j]*b;
			}

			return c;
		}



		su3_vector<T> operator-(su3_vector<T> b) {
			// #pragma HLS inline
			su3_vector<T> c;

			for (int j = 0; j < 3; j++) {
				#pragma HLS unroll
				c.v[j].i = v[j].i - b.v[j].i;
				c.v[j].r = v[j].r - b.v[j].r;
			}

			return c;
		}

		su3_vector<T> operator^(su3_vector<T> b) {
			// #pragma HLS inline
			su3_vector c;

			for (int j = 0; j < 3; j++) {
			#pragma HLS unroll
				c.v[j].i = static_cast<T>(0.5/kappa)*b.v[j].i - static_cast<T>(0.5)*v[j].i;
				c.v[j].r = static_cast<T>(0.5/kappa)*b.v[j].r - static_cast<T>(0.5)*v[j].r;
			}

			return c;
		}

		void print() {
			#ifndef __SYNTHESIS__
				for (int i = 0; i < 3; i++) {
					if (v[i].i*v[i].i + v[i].r*v[i].r > 10e-8)
						std::cerr<<v[i].i<<" "<<v[i].r<<std::endl;
				}
			#endif
		}
};


#endif
